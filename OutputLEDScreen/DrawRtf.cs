﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

public static class DrawRtfText
{
	[DllImport("USER32.dll")]
	private static extern Int32 SendMessage(IntPtr hWnd, int msg, int wParam, IntPtr lParam);
	private const int WM_USER = 0x400;
	private const int EM_FORMATRANGE = WM_USER + 57;

	[StructLayout(LayoutKind.Sequential)]
	private struct RECT
	{
		public int Left;
		public int Top;
		public int Right;
		public int Bottom;
	}

	[StructLayout(LayoutKind.Sequential)]
	private struct CHARRANGE
	{
		public int cpMin;
		public int cpMax;
	}

	[StructLayout(LayoutKind.Sequential)]
	private struct FORMATRANGE
	{
		public IntPtr hdc;
		public IntPtr hdcTarget;
		public RECT rc;
		public RECT rcPage;
		public CHARRANGE chrg;
	}

	private const double inch = 1440.0;
	
	public static Bitmap DrawToBitmap(RichTextBox rtb)
	{
		Bitmap bmp = new Bitmap(rtb.Width, rtb.Height);
		Graphics gr = Graphics.FromImage(bmp);

		Single DpiY = gr.DpiY;
		Single DpiX = gr.DpiX;

		IntPtr hDC = gr.GetHdc();
		FORMATRANGE fmtRange;
		RECT rect;
		int fromAPI;

		rect.Top = 0; rect.Left = 0;
		rect.Bottom = (int)(bmp.Height * inch / DpiY);
		rect.Right = (int)(bmp.Width * inch / DpiX);
		fmtRange.chrg.cpMin = 0;
		fmtRange.chrg.cpMax = -1;
		fmtRange.hdc = hDC;
		fmtRange.hdcTarget = hDC;
		fmtRange.rc = rect;
		fmtRange.rcPage = rect;

		int wParam = 1;

		IntPtr lParam = Marshal.AllocCoTaskMem(Marshal.SizeOf(fmtRange));

		Marshal.StructureToPtr(fmtRange, lParam, false);
		fromAPI = SendMessage(rtb.Handle, EM_FORMATRANGE, wParam, lParam);
		Marshal.FreeCoTaskMem(lParam);

		fromAPI = SendMessage(rtb.Handle, EM_FORMATRANGE, wParam, new IntPtr(0));

		gr.ReleaseHdc(hDC);

		return bmp;
	}
}