﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutputLEDScreen
{
	public partial class LoginForm : Form
	{
		string[] strPasswords;

		public LoginForm()
		{
			InitializeComponent();

			strPasswords = new string[] { "user", "admin" };

			try
			{
				strPasswords = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"User.Dat"));

				if (strPasswords.Length == 0)
					strPasswords = new string[] { "user", "admin" };
				else if (strPasswords.Length == 1)
					strPasswords = new string[] { strPasswords[0], "admin" };
			}
			catch { }
		}

		private void LoginForm_Load(object sender, EventArgs e)
		{
			cmbUser.SelectedIndex = 0;
		}

		private void btnLogin_Click(object sender, EventArgs e)
		{
			if (cmbUser.SelectedIndex == 0 && txtPassword.Text == strPasswords[0]
				|| cmbUser.SelectedIndex == 1 && txtPassword.Text == strPasswords[1])
			{
				this.Hide();
				MainForm frm = new MainForm();
				frm.SetAdmin(cmbUser.SelectedIndex == 1);
				frm.ShowDialog();
				this.Close();
			}

			MessageBox.Show("User and Password are invalid.");
			txtPassword.Focus();
		}

		private void LoginForm_Shown(object sender, EventArgs e)
		{
			txtPassword.Focus();
		}

		private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13)
				btnLogin_Click(sender, null);
		}
	}
}
