﻿namespace OutputLEDScreen
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.rtbRealMsg = new System.Windows.Forms.RichTextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.rtbTextAway = new System.Windows.Forms.RichTextBox();
			this.toolStrip2 = new System.Windows.Forms.ToolStrip();
			this.cmbFontsAway = new System.Windows.Forms.ToolStripComboBox();
			this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
			this.btnIncFontAway = new System.Windows.Forms.ToolStripButton();
			this.btnDecFontAway = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
			this.btnBoldFontAway = new System.Windows.Forms.ToolStripButton();
			this.btnItalFontAway = new System.Windows.Forms.ToolStripButton();
			this.btnUndrFontAway = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
			this.btnBackColorPickAway = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.btnForeColorPickAway = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.nudTimeScroll = new System.Windows.Forms.NumericUpDown();
			this.rtbTextMsg = new System.Windows.Forms.RichTextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.cmbFontsMsg = new System.Windows.Forms.ToolStripComboBox();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.btnIncFontMsg = new System.Windows.Forms.ToolStripButton();
			this.btnDecFontMsg = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.btnBoldFontMsg = new System.Windows.Forms.ToolStripButton();
			this.btnItalFontMsg = new System.Windows.Forms.ToolStripButton();
			this.btnUndrFontMsg = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
			this.btnBackColorPickMsg = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.btnForeColorPickMsg = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.rtbTextHome = new System.Windows.Forms.RichTextBox();
			this.toolStrip3 = new System.Windows.Forms.ToolStrip();
			this.cmbFontsHome = new System.Windows.Forms.ToolStripComboBox();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.btnIncFontHome = new System.Windows.Forms.ToolStripButton();
			this.btnDecFontHome = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.btnBoldFontHome = new System.Windows.Forms.ToolStripButton();
			this.btnItalFontHome = new System.Windows.Forms.ToolStripButton();
			this.btnUndrFontHome = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.btnBackColorPickHome = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.btnForeColorPickHome = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.label1 = new System.Windows.Forms.Label();
			this.btnHotkeyVideo3 = new System.Windows.Forms.Button();
			this.btnHotkeyVideo2 = new System.Windows.Forms.Button();
			this.btnHotkeyVideo1 = new System.Windows.Forms.Button();
			this.btnSettings = new System.Windows.Forms.Button();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.groupBox3.SuspendLayout();
			this.toolStrip2.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeScroll)).BeginInit();
			this.toolStrip1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.toolStrip3.SuspendLayout();
			this.SuspendLayout();
			// 
			// rtbRealMsg
			// 
			this.rtbRealMsg.AutoWordSelection = true;
			this.rtbRealMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.rtbRealMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rtbRealMsg.Location = new System.Drawing.Point(354, 8);
			this.rtbRealMsg.Multiline = false;
			this.rtbRealMsg.Name = "rtbRealMsg";
			this.rtbRealMsg.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.rtbRealMsg.Size = new System.Drawing.Size(269, 48);
			this.rtbRealMsg.TabIndex = 9;
			this.rtbRealMsg.Text = "Real-Time Msg";
			this.rtbRealMsg.Visible = false;
			this.rtbRealMsg.WordWrap = false;
			this.rtbRealMsg.ContentsResized += new System.Windows.Forms.ContentsResizedEventHandler(this.richTextBox_ContentsResized);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.rtbTextAway);
			this.groupBox3.Controls.Add(this.toolStrip2);
			this.groupBox3.Controls.Add(this.label3);
			this.groupBox3.Location = new System.Drawing.Point(12, 188);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(442, 170);
			this.groupBox3.TabIndex = 1;
			this.groupBox3.TabStop = false;
			// 
			// rtbTextAway
			// 
			this.rtbTextAway.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.rtbTextAway.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rtbTextAway.Location = new System.Drawing.Point(13, 60);
			this.rtbTextAway.Multiline = false;
			this.rtbTextAway.Name = "rtbTextAway";
			this.rtbTextAway.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.rtbTextAway.Size = new System.Drawing.Size(412, 50);
			this.rtbTextAway.TabIndex = 5;
			this.rtbTextAway.Text = "User Input Text Box";
			this.rtbTextAway.SelectionChanged += new System.EventHandler(this.rtbTextAway_SelectionChanged);
			this.rtbTextAway.TextChanged += new System.EventHandler(this.rtbTextAway_TextChanged);
			// 
			// toolStrip2
			// 
			this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
			this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbFontsAway,
            this.toolStripSeparator7,
            this.btnIncFontAway,
            this.btnDecFontAway,
            this.toolStripSeparator8,
            this.btnBoldFontAway,
            this.btnItalFontAway,
            this.btnUndrFontAway,
            this.toolStripSeparator9,
            this.btnBackColorPickAway,
            this.btnForeColorPickAway});
			this.toolStrip2.Location = new System.Drawing.Point(13, 125);
			this.toolStrip2.Name = "toolStrip2";
			this.toolStrip2.Padding = new System.Windows.Forms.Padding(0);
			this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip2.Size = new System.Drawing.Size(412, 31);
			this.toolStrip2.TabIndex = 4;
			// 
			// cmbFontsAway
			// 
			this.cmbFontsAway.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.cmbFontsAway.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cmbFontsAway.Name = "cmbFontsAway";
			this.cmbFontsAway.Size = new System.Drawing.Size(150, 31);
			this.cmbFontsAway.SelectedIndexChanged += new System.EventHandler(this.cmbFontsAway_SelectedIndexChanged);
			// 
			// toolStripSeparator7
			// 
			this.toolStripSeparator7.Name = "toolStripSeparator7";
			this.toolStripSeparator7.Size = new System.Drawing.Size(6, 31);
			// 
			// btnIncFontAway
			// 
			this.btnIncFontAway.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnIncFontAway.Image = global::OutputLEDScreen.Properties.Resources.incfont;
			this.btnIncFontAway.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnIncFontAway.Name = "btnIncFontAway";
			this.btnIncFontAway.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnIncFontAway.Size = new System.Drawing.Size(32, 28);
			this.btnIncFontAway.Text = "toolStripButton4";
			this.btnIncFontAway.Click += new System.EventHandler(this.btnIncFontAway_Click);
			// 
			// btnDecFontAway
			// 
			this.btnDecFontAway.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnDecFontAway.Image = global::OutputLEDScreen.Properties.Resources.decfont;
			this.btnDecFontAway.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDecFontAway.Name = "btnDecFontAway";
			this.btnDecFontAway.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnDecFontAway.Size = new System.Drawing.Size(32, 28);
			this.btnDecFontAway.Text = "toolStripButton5";
			this.btnDecFontAway.Click += new System.EventHandler(this.btnDecFontAway_Click);
			// 
			// toolStripSeparator8
			// 
			this.toolStripSeparator8.Name = "toolStripSeparator8";
			this.toolStripSeparator8.Size = new System.Drawing.Size(6, 31);
			// 
			// btnBoldFontAway
			// 
			this.btnBoldFontAway.CheckOnClick = true;
			this.btnBoldFontAway.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBoldFontAway.Image = ((System.Drawing.Image)(resources.GetObject("btnBoldFontAway.Image")));
			this.btnBoldFontAway.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBoldFontAway.Name = "btnBoldFontAway";
			this.btnBoldFontAway.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnBoldFontAway.Size = new System.Drawing.Size(32, 28);
			this.btnBoldFontAway.Text = "toolStripButton1";
			this.btnBoldFontAway.Click += new System.EventHandler(this.btnBoldFontAway_Click);
			// 
			// btnItalFontAway
			// 
			this.btnItalFontAway.CheckOnClick = true;
			this.btnItalFontAway.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnItalFontAway.Image = ((System.Drawing.Image)(resources.GetObject("btnItalFontAway.Image")));
			this.btnItalFontAway.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnItalFontAway.Name = "btnItalFontAway";
			this.btnItalFontAway.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnItalFontAway.Size = new System.Drawing.Size(32, 28);
			this.btnItalFontAway.Text = "toolStripButton2";
			this.btnItalFontAway.Click += new System.EventHandler(this.btnItalFontAway_Click);
			// 
			// btnUndrFontAway
			// 
			this.btnUndrFontAway.CheckOnClick = true;
			this.btnUndrFontAway.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnUndrFontAway.Image = ((System.Drawing.Image)(resources.GetObject("btnUndrFontAway.Image")));
			this.btnUndrFontAway.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnUndrFontAway.Name = "btnUndrFontAway";
			this.btnUndrFontAway.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnUndrFontAway.Size = new System.Drawing.Size(32, 28);
			this.btnUndrFontAway.Text = "toolStripButton3";
			this.btnUndrFontAway.Click += new System.EventHandler(this.btnUndrFontAway_Click);
			// 
			// toolStripSeparator9
			// 
			this.toolStripSeparator9.Name = "toolStripSeparator9";
			this.toolStripSeparator9.Size = new System.Drawing.Size(6, 31);
			// 
			// btnBackColorPickAway
			// 
			this.btnBackColorPickAway.Color = System.Drawing.Color.Yellow;
			this.btnBackColorPickAway.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBackColorPickAway.Image = ((System.Drawing.Image)(resources.GetObject("btnBackColorPickAway.Image")));
			this.btnBackColorPickAway.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBackColorPickAway.Name = "btnBackColorPickAway";
			this.btnBackColorPickAway.Size = new System.Drawing.Size(40, 28);
			this.btnBackColorPickAway.Text = "toolStripColorPickerSplitButton1";
			this.btnBackColorPickAway.ColorChanged += new System.EventHandler(this.btnBackColorPickAway_ColorChanged);
			// 
			// btnForeColorPickAway
			// 
			this.btnForeColorPickAway.Color = System.Drawing.Color.Red;
			this.btnForeColorPickAway.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnForeColorPickAway.Image = ((System.Drawing.Image)(resources.GetObject("btnForeColorPickAway.Image")));
			this.btnForeColorPickAway.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnForeColorPickAway.Name = "btnForeColorPickAway";
			this.btnForeColorPickAway.Size = new System.Drawing.Size(40, 28);
			this.btnForeColorPickAway.Text = "toolStripColorPickerSplitButton2";
			this.btnForeColorPickAway.ColorChanged += new System.EventHandler(this.btnForeColorPickAway_ColorChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(7, 17);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(175, 31);
			this.label3.TabIndex = 0;
			this.label3.Text = "AWAY TEAM";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.nudTimeScroll);
			this.groupBox2.Controls.Add(this.rtbTextMsg);
			this.groupBox2.Controls.Add(this.rtbRealMsg);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.toolStrip1);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Location = new System.Drawing.Point(465, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(623, 346);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			// 
			// nudTimeScroll
			// 
			this.nudTimeScroll.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudTimeScroll.Location = new System.Drawing.Point(563, 302);
			this.nudTimeScroll.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.nudTimeScroll.Name = "nudTimeScroll";
			this.nudTimeScroll.Size = new System.Drawing.Size(48, 29);
			this.nudTimeScroll.TabIndex = 6;
			this.nudTimeScroll.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
			this.nudTimeScroll.ValueChanged += new System.EventHandler(this.nudTimeScroll_ValueChanged);
			// 
			// rtbTextMsg
			// 
			this.rtbTextMsg.AutoWordSelection = true;
			this.rtbTextMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.rtbTextMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rtbTextMsg.Location = new System.Drawing.Point(13, 60);
			this.rtbTextMsg.Name = "rtbTextMsg";
			this.rtbTextMsg.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.rtbTextMsg.Size = new System.Drawing.Size(598, 226);
			this.rtbTextMsg.TabIndex = 5;
			this.rtbTextMsg.Text = "User Input Text Box";
			this.rtbTextMsg.SelectionChanged += new System.EventHandler(this.rtbTextMsg_SelectionChanged);
			this.rtbTextMsg.TextChanged += new System.EventHandler(this.rtbTextMsg_TextChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(429, 303);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(134, 25);
			this.label4.TabIndex = 0;
			this.label4.Text = "Scroll Speed";
			// 
			// toolStrip1
			// 
			this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
			this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbFontsMsg,
            this.toolStripSeparator4,
            this.btnIncFontMsg,
            this.btnDecFontMsg,
            this.toolStripSeparator5,
            this.btnBoldFontMsg,
            this.btnItalFontMsg,
            this.btnUndrFontMsg,
            this.toolStripSeparator6,
            this.btnBackColorPickMsg,
            this.btnForeColorPickMsg});
			this.toolStrip1.Location = new System.Drawing.Point(13, 301);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
			this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip1.Size = new System.Drawing.Size(412, 31);
			this.toolStrip1.TabIndex = 4;
			// 
			// cmbFontsMsg
			// 
			this.cmbFontsMsg.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.cmbFontsMsg.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cmbFontsMsg.Name = "cmbFontsMsg";
			this.cmbFontsMsg.Size = new System.Drawing.Size(150, 31);
			this.cmbFontsMsg.SelectedIndexChanged += new System.EventHandler(this.cmbFontsMsg_SelectedIndexChanged);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
			// 
			// btnIncFontMsg
			// 
			this.btnIncFontMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnIncFontMsg.Image = global::OutputLEDScreen.Properties.Resources.incfont;
			this.btnIncFontMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnIncFontMsg.Name = "btnIncFontMsg";
			this.btnIncFontMsg.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnIncFontMsg.Size = new System.Drawing.Size(32, 28);
			this.btnIncFontMsg.Text = "toolStripButton4";
			this.btnIncFontMsg.Click += new System.EventHandler(this.btnIncFontMsg_Click);
			// 
			// btnDecFontMsg
			// 
			this.btnDecFontMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnDecFontMsg.Image = global::OutputLEDScreen.Properties.Resources.decfont;
			this.btnDecFontMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDecFontMsg.Name = "btnDecFontMsg";
			this.btnDecFontMsg.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnDecFontMsg.Size = new System.Drawing.Size(32, 28);
			this.btnDecFontMsg.Text = "toolStripButton5";
			this.btnDecFontMsg.Click += new System.EventHandler(this.btnDecFontMsg_Click);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(6, 31);
			// 
			// btnBoldFontMsg
			// 
			this.btnBoldFontMsg.CheckOnClick = true;
			this.btnBoldFontMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBoldFontMsg.Image = ((System.Drawing.Image)(resources.GetObject("btnBoldFontMsg.Image")));
			this.btnBoldFontMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBoldFontMsg.Name = "btnBoldFontMsg";
			this.btnBoldFontMsg.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnBoldFontMsg.Size = new System.Drawing.Size(32, 28);
			this.btnBoldFontMsg.Text = "toolStripButton1";
			this.btnBoldFontMsg.Click += new System.EventHandler(this.btnBoldFontMsg_Click);
			// 
			// btnItalFontMsg
			// 
			this.btnItalFontMsg.CheckOnClick = true;
			this.btnItalFontMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnItalFontMsg.Image = ((System.Drawing.Image)(resources.GetObject("btnItalFontMsg.Image")));
			this.btnItalFontMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnItalFontMsg.Name = "btnItalFontMsg";
			this.btnItalFontMsg.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnItalFontMsg.Size = new System.Drawing.Size(32, 28);
			this.btnItalFontMsg.Text = "toolStripButton2";
			this.btnItalFontMsg.Click += new System.EventHandler(this.btnItalFontMsg_Click);
			// 
			// btnUndrFontMsg
			// 
			this.btnUndrFontMsg.CheckOnClick = true;
			this.btnUndrFontMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnUndrFontMsg.Image = ((System.Drawing.Image)(resources.GetObject("btnUndrFontMsg.Image")));
			this.btnUndrFontMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnUndrFontMsg.Name = "btnUndrFontMsg";
			this.btnUndrFontMsg.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnUndrFontMsg.Size = new System.Drawing.Size(32, 28);
			this.btnUndrFontMsg.Text = "toolStripButton3";
			this.btnUndrFontMsg.Click += new System.EventHandler(this.btnUndrFontMsg_Click);
			// 
			// toolStripSeparator6
			// 
			this.toolStripSeparator6.Name = "toolStripSeparator6";
			this.toolStripSeparator6.Size = new System.Drawing.Size(6, 31);
			// 
			// btnBackColorPickMsg
			// 
			this.btnBackColorPickMsg.Color = System.Drawing.Color.Yellow;
			this.btnBackColorPickMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBackColorPickMsg.Image = ((System.Drawing.Image)(resources.GetObject("btnBackColorPickMsg.Image")));
			this.btnBackColorPickMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBackColorPickMsg.Name = "btnBackColorPickMsg";
			this.btnBackColorPickMsg.Size = new System.Drawing.Size(40, 28);
			this.btnBackColorPickMsg.Text = "toolStripColorPickerSplitButton1";
			this.btnBackColorPickMsg.ColorChanged += new System.EventHandler(this.btnBackColorPickMsg_ColorChanged);
			// 
			// btnForeColorPickMsg
			// 
			this.btnForeColorPickMsg.Color = System.Drawing.Color.Red;
			this.btnForeColorPickMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnForeColorPickMsg.Image = ((System.Drawing.Image)(resources.GetObject("btnForeColorPickMsg.Image")));
			this.btnForeColorPickMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnForeColorPickMsg.Name = "btnForeColorPickMsg";
			this.btnForeColorPickMsg.Size = new System.Drawing.Size(40, 28);
			this.btnForeColorPickMsg.Text = "toolStripColorPickerSplitButton2";
			this.btnForeColorPickMsg.ColorChanged += new System.EventHandler(this.btnForeColorPickMsg_ColorChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(7, 17);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(312, 31);
			this.label2.TabIndex = 0;
			this.label2.Text = "SCROLLING MESSAGE";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.rtbTextHome);
			this.groupBox1.Controls.Add(this.toolStrip3);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(442, 170);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			// 
			// rtbTextHome
			// 
			this.rtbTextHome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.rtbTextHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rtbTextHome.Location = new System.Drawing.Point(13, 60);
			this.rtbTextHome.Multiline = false;
			this.rtbTextHome.Name = "rtbTextHome";
			this.rtbTextHome.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.rtbTextHome.Size = new System.Drawing.Size(412, 50);
			this.rtbTextHome.TabIndex = 5;
			this.rtbTextHome.Text = "User Input Text Box";
			this.rtbTextHome.SelectionChanged += new System.EventHandler(this.rtbTextHome_SelectionChanged);
			this.rtbTextHome.TextChanged += new System.EventHandler(this.rtbTextHome_TextChanged);
			// 
			// toolStrip3
			// 
			this.toolStrip3.BackColor = System.Drawing.SystemColors.Control;
			this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip3.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbFontsHome,
            this.toolStripSeparator3,
            this.btnIncFontHome,
            this.btnDecFontHome,
            this.toolStripSeparator2,
            this.btnBoldFontHome,
            this.btnItalFontHome,
            this.btnUndrFontHome,
            this.toolStripSeparator1,
            this.btnBackColorPickHome,
            this.btnForeColorPickHome});
			this.toolStrip3.Location = new System.Drawing.Point(13, 125);
			this.toolStrip3.Name = "toolStrip3";
			this.toolStrip3.Padding = new System.Windows.Forms.Padding(0);
			this.toolStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip3.Size = new System.Drawing.Size(412, 31);
			this.toolStrip3.TabIndex = 4;
			// 
			// cmbFontsHome
			// 
			this.cmbFontsHome.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.cmbFontsHome.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cmbFontsHome.Name = "cmbFontsHome";
			this.cmbFontsHome.Size = new System.Drawing.Size(150, 31);
			this.cmbFontsHome.SelectedIndexChanged += new System.EventHandler(this.cmbFontsHome_SelectedIndexChanged);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
			// 
			// btnIncFontHome
			// 
			this.btnIncFontHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnIncFontHome.Image = global::OutputLEDScreen.Properties.Resources.incfont;
			this.btnIncFontHome.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnIncFontHome.Name = "btnIncFontHome";
			this.btnIncFontHome.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnIncFontHome.Size = new System.Drawing.Size(32, 28);
			this.btnIncFontHome.Text = "toolStripButton4";
			this.btnIncFontHome.Click += new System.EventHandler(this.btnIncFontHome_Click);
			// 
			// btnDecFontHome
			// 
			this.btnDecFontHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnDecFontHome.Image = global::OutputLEDScreen.Properties.Resources.decfont;
			this.btnDecFontHome.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDecFontHome.Name = "btnDecFontHome";
			this.btnDecFontHome.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnDecFontHome.Size = new System.Drawing.Size(32, 28);
			this.btnDecFontHome.Text = "toolStripButton5";
			this.btnDecFontHome.Click += new System.EventHandler(this.btnDecFontHome_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
			// 
			// btnBoldFontHome
			// 
			this.btnBoldFontHome.CheckOnClick = true;
			this.btnBoldFontHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBoldFontHome.Image = ((System.Drawing.Image)(resources.GetObject("btnBoldFontHome.Image")));
			this.btnBoldFontHome.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBoldFontHome.Name = "btnBoldFontHome";
			this.btnBoldFontHome.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnBoldFontHome.Size = new System.Drawing.Size(32, 28);
			this.btnBoldFontHome.Text = "toolStripButton1";
			this.btnBoldFontHome.Click += new System.EventHandler(this.btnBoldFontHome_Click);
			// 
			// btnItalFontHome
			// 
			this.btnItalFontHome.CheckOnClick = true;
			this.btnItalFontHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnItalFontHome.Image = ((System.Drawing.Image)(resources.GetObject("btnItalFontHome.Image")));
			this.btnItalFontHome.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnItalFontHome.Name = "btnItalFontHome";
			this.btnItalFontHome.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnItalFontHome.Size = new System.Drawing.Size(32, 28);
			this.btnItalFontHome.Text = "toolStripButton2";
			this.btnItalFontHome.Click += new System.EventHandler(this.btnItalFontHome_Click);
			// 
			// btnUndrFontHome
			// 
			this.btnUndrFontHome.CheckOnClick = true;
			this.btnUndrFontHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnUndrFontHome.Image = ((System.Drawing.Image)(resources.GetObject("btnUndrFontHome.Image")));
			this.btnUndrFontHome.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnUndrFontHome.Name = "btnUndrFontHome";
			this.btnUndrFontHome.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.btnUndrFontHome.Size = new System.Drawing.Size(32, 28);
			this.btnUndrFontHome.Text = "toolStripButton3";
			this.btnUndrFontHome.Click += new System.EventHandler(this.btnUndrFontHome_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
			// 
			// btnBackColorPickHome
			// 
			this.btnBackColorPickHome.Color = System.Drawing.Color.Yellow;
			this.btnBackColorPickHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBackColorPickHome.Image = ((System.Drawing.Image)(resources.GetObject("btnBackColorPickHome.Image")));
			this.btnBackColorPickHome.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBackColorPickHome.Name = "btnBackColorPickHome";
			this.btnBackColorPickHome.Size = new System.Drawing.Size(40, 28);
			this.btnBackColorPickHome.Text = "toolStripColorPickerSplitButton1";
			this.btnBackColorPickHome.ColorChanged += new System.EventHandler(this.btnBackColorPickHome_ColorChanged);
			// 
			// btnForeColorPickHome
			// 
			this.btnForeColorPickHome.Color = System.Drawing.Color.Red;
			this.btnForeColorPickHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnForeColorPickHome.Image = ((System.Drawing.Image)(resources.GetObject("btnForeColorPickHome.Image")));
			this.btnForeColorPickHome.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnForeColorPickHome.Name = "btnForeColorPickHome";
			this.btnForeColorPickHome.Size = new System.Drawing.Size(40, 28);
			this.btnForeColorPickHome.Text = "toolStripColorPickerSplitButton2";
			this.btnForeColorPickHome.ColorChanged += new System.EventHandler(this.btnForeColorPickHome_ColorChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(7, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(177, 31);
			this.label1.TabIndex = 0;
			this.label1.Text = "HOME TEAM";
			// 
			// btnHotkeyVideo3
			// 
			this.btnHotkeyVideo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnHotkeyVideo3.Location = new System.Drawing.Point(488, 371);
			this.btnHotkeyVideo3.Name = "btnHotkeyVideo3";
			this.btnHotkeyVideo3.Size = new System.Drawing.Size(179, 61);
			this.btnHotkeyVideo3.TabIndex = 0;
			this.btnHotkeyVideo3.Text = "Video 3 Hotkey";
			this.btnHotkeyVideo3.UseVisualStyleBackColor = true;
			this.btnHotkeyVideo3.Click += new System.EventHandler(this.btnHotkeyVideo3_Click);
			// 
			// btnHotkeyVideo2
			// 
			this.btnHotkeyVideo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnHotkeyVideo2.Location = new System.Drawing.Point(257, 371);
			this.btnHotkeyVideo2.Name = "btnHotkeyVideo2";
			this.btnHotkeyVideo2.Size = new System.Drawing.Size(179, 61);
			this.btnHotkeyVideo2.TabIndex = 0;
			this.btnHotkeyVideo2.Text = "Video 2 Hotkey";
			this.btnHotkeyVideo2.UseVisualStyleBackColor = true;
			this.btnHotkeyVideo2.Click += new System.EventHandler(this.btnHotkeyVideo2_Click);
			// 
			// btnHotkeyVideo1
			// 
			this.btnHotkeyVideo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnHotkeyVideo1.Location = new System.Drawing.Point(26, 371);
			this.btnHotkeyVideo1.Name = "btnHotkeyVideo1";
			this.btnHotkeyVideo1.Size = new System.Drawing.Size(179, 61);
			this.btnHotkeyVideo1.TabIndex = 0;
			this.btnHotkeyVideo1.Text = "Video 1 Hotkey";
			this.btnHotkeyVideo1.UseVisualStyleBackColor = true;
			this.btnHotkeyVideo1.Click += new System.EventHandler(this.btnHotkeyVideo1_Click);
			// 
			// btnSettings
			// 
			this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
			this.btnSettings.Location = new System.Drawing.Point(909, 371);
			this.btnSettings.Name = "btnSettings";
			this.btnSettings.Size = new System.Drawing.Size(179, 61);
			this.btnSettings.TabIndex = 0;
			this.btnSettings.Text = "Settings";
			this.btnSettings.UseVisualStyleBackColor = true;
			this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1104, 446);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.btnSettings);
			this.Controls.Add(this.btnHotkeyVideo3);
			this.Controls.Add(this.btnHotkeyVideo1);
			this.Controls.Add(this.btnHotkeyVideo2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Location = new System.Drawing.Point(0, 300);
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "GUI Screen";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Shown += new System.EventHandler(this.MainForm_Shown);
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.toolStrip2.ResumeLayout(false);
			this.toolStrip2.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeScroll)).EndInit();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.toolStrip3.ResumeLayout(false);
			this.toolStrip3.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button btnSettings;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.FontDialog fontDialog1;
		private System.Windows.Forms.ToolStrip toolStrip3;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnBackColorPickHome;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnForeColorPickHome;
		private System.Windows.Forms.ToolStripButton btnBoldFontHome;
		private System.Windows.Forms.ToolStripButton btnItalFontHome;
		private System.Windows.Forms.ToolStripButton btnUndrFontHome;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripComboBox cmbFontsHome;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton btnIncFontHome;
		private System.Windows.Forms.ToolStripButton btnDecFontHome;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.RichTextBox rtbTextHome;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.RichTextBox rtbTextAway;
		private System.Windows.Forms.ToolStrip toolStrip2;
		private System.Windows.Forms.ToolStripComboBox cmbFontsAway;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
		private System.Windows.Forms.ToolStripButton btnIncFontAway;
		private System.Windows.Forms.ToolStripButton btnDecFontAway;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
		private System.Windows.Forms.ToolStripButton btnBoldFontAway;
		private System.Windows.Forms.ToolStripButton btnItalFontAway;
		private System.Windows.Forms.ToolStripButton btnUndrFontAway;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnBackColorPickAway;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnForeColorPickAway;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RichTextBox rtbTextMsg;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripComboBox cmbFontsMsg;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripButton btnIncFontMsg;
		private System.Windows.Forms.ToolStripButton btnDecFontMsg;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripButton btnBoldFontMsg;
		private System.Windows.Forms.ToolStripButton btnItalFontMsg;
		private System.Windows.Forms.ToolStripButton btnUndrFontMsg;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnBackColorPickMsg;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnForeColorPickMsg;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnHotkeyVideo1;
		private System.Windows.Forms.Button btnHotkeyVideo3;
		private System.Windows.Forms.Button btnHotkeyVideo2;
		private System.Windows.Forms.NumericUpDown nudTimeScroll;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.RichTextBox rtbRealMsg;
	}
}