﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.Structure;

namespace OutputLEDScreen
{

	public partial class MainForm : Form
	{
		private SettingsInfo _settingsInfo;
		private OutputForm outputForm;
		bool isFormLoaded;
		private bool isAdmin;

		#region Form Control

		public MainForm()
		{
			isFormLoaded = false;
			isAdmin = false;

			InitializeComponent();
		}
		
		public void SetAdmin(bool admin)
		{
			isAdmin = admin;

			if (!isAdmin)
			{
				btnSettings.Enabled = false;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			_settingsInfo = new SettingsInfo();

			foreach (FontFamily oneFontFamily in FontFamily.Families)
			{
				cmbFontsHome.Items.Add(oneFontFamily.Name);
				cmbFontsAway.Items.Add(oneFontFamily.Name);
				cmbFontsMsg.Items.Add(oneFontFamily.Name);
			}

			cmbFontsHome.Items.RemoveAt(0);
			cmbFontsAway.Items.RemoveAt(0);
			cmbFontsMsg.Items.RemoveAt(0);

			cmbFontsHome.SelectedText = _settingsInfo.TextInfos[0].fontName;
			btnBackColorPickHome.Color = _settingsInfo.TextInfos[0].backColor;
			btnForeColorPickHome.Color = _settingsInfo.TextInfos[0].foreColor;

			cmbFontsAway.SelectedText = _settingsInfo.TextInfos[1].fontName;
			btnBackColorPickAway.Color = _settingsInfo.TextInfos[1].backColor;
			btnForeColorPickAway.Color = _settingsInfo.TextInfos[1].foreColor;

			cmbFontsMsg.SelectedText = _settingsInfo.TextInfos[2].fontName;
			btnBackColorPickMsg.Color = _settingsInfo.TextInfos[2].backColor;
			//btnForeColorPickMsg.Color = _settingsInfo.TextInfos[2].foreColor;

			SetScreenControls();

			outputForm = new OutputForm();
			outputForm.SetSettingsInfo(_settingsInfo);
			outputForm.Show();

			//this.Left = 600;

			base.OnLoad(e);
		}

		private void MainForm_Shown(object sender, EventArgs e)
		{
			isFormLoaded = true;

			SetStaticImage(rtbTextHome, 1);
			SetStaticImage(rtbTextAway, 2);

			SetScrollMsg();
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (MessageBox.Show("Close program?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
			{
				e.Cancel = true;
				this.Activate();
				return;
			}

			outputForm.Close();
			outputForm.Dispose();
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			_settingsInfo.TextInfos[0].displayText = rtbTextHome.Rtf;
			_settingsInfo.TextInfos[0].backColor = rtbTextHome.BackColor;
			_settingsInfo.TextInfos[0].foreColor = btnForeColorPickHome.Color;

			_settingsInfo.TextInfos[1].displayText = rtbTextAway.Rtf;
			_settingsInfo.TextInfos[1].backColor = rtbTextAway.BackColor;
			_settingsInfo.TextInfos[1].foreColor = btnForeColorPickAway.Color;

			_settingsInfo.TextInfos[2].displayText = rtbTextMsg.Rtf;
			_settingsInfo.TextInfos[2].backColor = rtbTextMsg.BackColor;

			_settingsInfo.TextInfos[2].scrollSpeed = (int)nudTimeScroll.Value;

			_settingsInfo.Save();

			base.OnClosing(e);
		}

		private void SetScreenControls()
		{
			try
			{
				rtbTextHome.Rtf = _settingsInfo.TextInfos[0].displayText;

				rtbTextHome.SelectAll();
				rtbTextHome.Select(0, 0);

			}
			catch { }

			try
			{
				rtbTextAway.Rtf = _settingsInfo.TextInfos[1].displayText;

				rtbTextAway.SelectAll();
				rtbTextAway.Select(0, 0);
			}
			catch { }

			try {
				rtbTextMsg.Rtf = _settingsInfo.TextInfos[2].displayText;
				nudTimeScroll.Value = _settingsInfo.TextInfos[2].scrollSpeed;
			} catch { }

			// setting hotkey button
			btnHotkeyVideo1.Text = _settingsInfo.ItemInfos[1].displayName;
			btnHotkeyVideo1.BackColor = _settingsInfo.ItemInfos[1].backColor;
			btnHotkeyVideo1.ForeColor = _settingsInfo.ItemInfos[1].foreColor;
			btnHotkeyVideo1.Enabled = File.Exists(_settingsInfo.ItemInfos[1].dirPath);

			btnHotkeyVideo2.Text = _settingsInfo.ItemInfos[2].displayName;
			btnHotkeyVideo2.BackColor = _settingsInfo.ItemInfos[2].backColor;
			btnHotkeyVideo2.ForeColor = _settingsInfo.ItemInfos[2].foreColor;
			btnHotkeyVideo2.Enabled = File.Exists(_settingsInfo.ItemInfos[2].dirPath);

			btnHotkeyVideo3.Text = _settingsInfo.ItemInfos[3].displayName;
			btnHotkeyVideo3.BackColor = _settingsInfo.ItemInfos[3].backColor;
			btnHotkeyVideo3.ForeColor = _settingsInfo.ItemInfos[3].foreColor;
			btnHotkeyVideo3.Enabled = File.Exists(_settingsInfo.ItemInfos[3].dirPath);
		}

		private void btnSettings_Click(object sender, EventArgs e)
		{
            _settingsInfo.TextInfos[0].displayText = rtbTextHome.Rtf;
            _settingsInfo.TextInfos[1].displayText = rtbTextAway.Rtf;
            _settingsInfo.TextInfos[2].displayText = rtbTextMsg.Rtf;

            //this.TopMost = false;

            SettingsForm _settingsForm = new SettingsForm(_settingsInfo);

			//_settingsForm.TopMost = true;

			if (_settingsForm.ShowDialog() == DialogResult.OK)
			{
				_settingsInfo.Save();
				SetScreenControls();

				outputForm.SetSettingsInfo(_settingsInfo);
			}

			//_settingsForm.TopMost = false;
			//this.TopMost = true;
		}

		#endregion

		#region Chane Items

		// change font name
		private void SetNewFontToAllText(RichTextBox rtb, string newFontName = "", int incSize = 0)
		{
			rtb.SelectAll();
			try
			{
				rtb.SelectionFont = new Font((newFontName != string.Empty ? newFontName : rtb.SelectionFont.Name), rtb.SelectionFont.Size + incSize, rtb.SelectionFont.Style);
			}
			catch { }
			rtb.Select(0, 0);

			if (rtb.Equals(rtbTextHome))
				SetStaticImage(rtb, 1);
			else
				SetStaticImage(rtb, 2);
		}

		private void cmbFontsHome_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetNewFontToAllText(rtbTextHome, cmbFontsHome.Text);
		}

		private void cmbFontsAway_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetNewFontToAllText(rtbTextAway, cmbFontsAway.Text);
		}

		private void cmbFontsMsg_SelectedIndexChanged(object sender, EventArgs e)
		{
			rtbTextMsg.SelectionFont = new Font(cmbFontsMsg.Text, rtbTextAway.SelectionFont.Size, rtbTextAway.SelectionFont.Style);
			SetScrollMsg();
		}

		// increase font size
		private void btnIncFontHome_Click(object sender, EventArgs e)
		{
			SetNewFontToAllText(rtbTextHome, "", 1);
		}

		private void btnIncFontAway_Click(object sender, EventArgs e)
		{
			SetNewFontToAllText(rtbTextAway, "", 1);
		}

		private void btnIncFontMsg_Click(object sender, EventArgs e)
		{
			try
			{
				if (rtbTextMsg.SelectionFont == null)
					rtbTextMsg.Select(0, rtbRealMsg.Rtf.Length);

				rtbTextMsg.SelectionFont = new Font(rtbTextMsg.SelectionFont.Name, rtbTextMsg.SelectionFont.Size + 1, rtbTextMsg.SelectionFont.Style);
			}
			catch { }
			SetScrollMsg();
		}

		// decrease font size
		private void btnDecFontHome_Click(object sender, EventArgs e)
		{
			SetNewFontToAllText(rtbTextHome, "", -1);
		}

		private void btnDecFontAway_Click(object sender, EventArgs e)
		{
			SetNewFontToAllText(rtbTextAway, "", -1);
		}

		private void btnDecFontMsg_Click(object sender, EventArgs e)
		{
			try
			{
				if (rtbTextMsg.SelectionFont == null)
					rtbTextMsg.Select(0, rtbRealMsg.Rtf.Length);

				rtbTextMsg.SelectionFont = new Font(rtbTextMsg.SelectionFont.Name, rtbTextMsg.SelectionFont.Size > 2 ? rtbTextMsg.SelectionFont.Size - 1 : 1, rtbTextMsg.SelectionFont.Style);
			}
			catch { }
			SetScrollMsg();
		}

		private void SetNewFontStyle(RichTextBox rtb, Font curFont, FontStyle newFontStyle)
		{
			rtb.SelectAll();

			FontStyle curFontStyle = curFont.Style;

			curFontStyle ^= newFontStyle;

			rtb.SelectionFont = new Font(curFont.Name, curFont.Size, curFontStyle);
			rtb.Select(0, 0);

			if (rtb.Equals(rtbTextHome))
				SetStaticImage(rtb, 1);
			else
				SetStaticImage(rtb, 2);
		}

		// bold
		private void btnBoldFontHome_Click(object sender, EventArgs e)
		{
			SetNewFontStyle(rtbTextHome, rtbTextHome.SelectionFont, FontStyle.Bold);
		}

		private void btnBoldFontAway_Click(object sender, EventArgs e)
		{
			SetNewFontStyle(rtbTextAway, rtbTextAway.SelectionFont, FontStyle.Bold);
		}

		private void btnBoldFontMsg_Click(object sender, EventArgs e)
		{
			rtbTextMsg.SelectionFont = new Font(rtbTextMsg.SelectionFont.Name, rtbTextMsg.SelectionFont.Size, rtbTextMsg.SelectionFont.Style ^ FontStyle.Bold);
			SetScrollMsg();
		}

		// italic		
		private void btnItalFontHome_Click(object sender, EventArgs e)
		{
			SetNewFontStyle(rtbTextHome, rtbTextHome.SelectionFont, FontStyle.Italic);
		}

		private void btnItalFontAway_Click(object sender, EventArgs e)
		{
			SetNewFontStyle(rtbTextAway, rtbTextAway.SelectionFont, FontStyle.Italic);
		}

		private void btnItalFontMsg_Click(object sender, EventArgs e)
		{
			rtbTextMsg.SelectionFont = new Font(rtbTextMsg.SelectionFont.Name, rtbTextMsg.SelectionFont.Size, rtbTextMsg.SelectionFont.Style ^ FontStyle.Italic);
			SetScrollMsg();
		}

		// underline
		private void btnUndrFontHome_Click(object sender, EventArgs e)
		{
			SetNewFontStyle(rtbTextHome, rtbTextHome.SelectionFont, FontStyle.Underline);
		}

		private void btnUndrFontAway_Click(object sender, EventArgs e)
		{
			SetNewFontStyle(rtbTextAway, rtbTextAway.SelectionFont, FontStyle.Underline);
		}

		private void btnUndrFontMsg_Click(object sender, EventArgs e)
		{
			rtbTextMsg.SelectionFont = new Font(rtbTextMsg.SelectionFont.Name, rtbTextMsg.SelectionFont.Size, rtbTextMsg.SelectionFont.Style ^ FontStyle.Underline);
			SetScrollMsg();
		}
		
		// back color
		private void btnBackColorPickHome_ColorChanged(object sender, EventArgs e)
		{
			rtbTextHome.BackColor = btnBackColorPickHome.Color;

			SetStaticImage(rtbTextHome, 1);
		}

		private void btnBackColorPickAway_ColorChanged(object sender, EventArgs e)
		{
			rtbTextAway.BackColor = btnBackColorPickAway.Color;

			SetStaticImage(rtbTextAway, 2);
		}

		private void btnBackColorPickMsg_ColorChanged(object sender, EventArgs e)
		{
			rtbTextMsg.BackColor = btnBackColorPickMsg.Color;

			SetScrollMsg();
		}

		// fore color
		private void btnForeColorPickHome_ColorChanged(object sender, EventArgs e)
		{
			rtbTextHome.SelectAll();
			rtbTextHome.SelectionColor = btnForeColorPickHome.Color;
			rtbTextHome.Select(0, 0);

			SetStaticImage(rtbTextHome, 1);
		}

		private void btnForeColorPickAway_ColorChanged(object sender, EventArgs e)
		{
			rtbTextAway.SelectAll();
			rtbTextAway.SelectionColor = btnForeColorPickAway.Color;
			rtbTextAway.Select(0, 0);

			SetStaticImage(rtbTextAway, 2);
		}

		private void btnForeColorPickMsg_ColorChanged(object sender, EventArgs e)
		{
			rtbTextMsg.SelectionColor = btnForeColorPickMsg.Color;

			SetScrollMsg();
		}

		// font changed

		private void rtbTextHome_SelectionChanged(object sender, EventArgs e)
		{
			if (rtbTextHome.SelectionFont != null)
			{
				btnBoldFontHome.Checked = rtbTextHome.SelectionFont.Bold;
				btnItalFontHome.Checked = rtbTextHome.SelectionFont.Italic;
				btnUndrFontHome.Checked = rtbTextHome.SelectionFont.Underline;
			}
			else
			{
				btnBoldFontHome.Checked = false;
				btnItalFontHome.Checked = false;
				btnUndrFontHome.Checked = false;
			}

			rtbTextHome.SelectionAlignment = HorizontalAlignment.Center;
		}

		private void rtbTextAway_SelectionChanged(object sender, EventArgs e)
		{
			if (rtbTextAway.SelectionFont != null)
			{
				btnBoldFontAway.Checked = rtbTextAway.SelectionFont.Bold;
				btnItalFontAway.Checked = rtbTextAway.SelectionFont.Italic;
				btnUndrFontAway.Checked = rtbTextAway.SelectionFont.Underline;
			}
			else
			{
				btnBoldFontAway.Checked = false;
				btnItalFontAway.Checked = false;
				btnUndrFontAway.Checked = false;
			}

			rtbTextAway.SelectionAlignment = HorizontalAlignment.Center;
		}
		private void rtbTextMsg_SelectionChanged(object sender, EventArgs e)
		{
			if (rtbTextMsg.SelectionFont != null)
			{
				cmbFontsMsg.Text = rtbTextMsg.SelectionFont.Name;

				btnBoldFontMsg.Checked = rtbTextMsg.SelectionFont.Bold;
				btnItalFontMsg.Checked = rtbTextMsg.SelectionFont.Italic;
				btnUndrFontMsg.Checked = rtbTextMsg.SelectionFont.Underline;
			}
		}

		private void rtbTextHome_TextChanged(object sender, EventArgs e)
		{
			SetStaticImage(rtbTextHome, 1);
		}

		private void rtbTextAway_TextChanged(object sender, EventArgs e)
		{
			SetStaticImage(rtbTextAway, 2);
		}

		private void rtbTextMsg_TextChanged(object sender, EventArgs e)
		{
			SetScrollMsg();
		}

		#endregion

		#region Hotkey Video

		private void btnHotkeyVideo1_Click(object sender, EventArgs e)
		{
			SetVideo(1);
		}

		private void btnHotkeyVideo2_Click(object sender, EventArgs e)
		{
			SetVideo(2);
		}
		
		private void btnHotkeyVideo3_Click(object sender, EventArgs e)
		{
			SetVideo(3);
		}

		private void SetVideo(int index)
		{
			outputForm.SetVideo(index);
		}

		#endregion

		#region Display Image

		private void SetStaticImage(RichTextBox rtb, int index)
		{
			if (!isFormLoaded)
				return;

			rtbRealMsg.Rtf = rtb.Rtf;
			rtbRealMsg.BackColor = rtb.BackColor;

			outputForm.SetStaticImage(rtbRealMsg, index);
		}

		private void SetScrollMsg()
		{
			if (!isFormLoaded)
				return;

			rtbRealMsg.Rtf = rtbTextMsg.Rtf;
			rtbRealMsg.BackColor = rtbTextMsg.BackColor;

			outputForm.SetScrollMsg(rtbRealMsg);
		}

		private void nudTimeScroll_ValueChanged(object sender, EventArgs e)
		{
			_settingsInfo.TextInfos[2].scrollSpeed = (int)nudTimeScroll.Value;
		}

		private void richTextBox_ContentsResized(object sender, ContentsResizedEventArgs e)
		{
			RichTextBox richTextBox = (RichTextBox)sender;
			richTextBox.Width = e.NewRectangle.Width;
			richTextBox.Height = e.NewRectangle.Height;
			richTextBox.Width += 1;
			richTextBox.Height += 0;
		}

		#endregion

	}
}
