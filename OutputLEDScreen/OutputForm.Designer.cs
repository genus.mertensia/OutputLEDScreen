﻿namespace OutputLEDScreen
{
	partial class OutputForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.tmrShowImage = new System.Windows.Forms.Timer(this.components);
			this.picScreen4 = new System.Windows.Forms.PictureBox();
			this.picScreen3 = new System.Windows.Forms.PictureBox();
			this.picScreen2 = new System.Windows.Forms.PictureBox();
			this.picScreen1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.picScreen4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picScreen3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picScreen2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picScreen1)).BeginInit();
			this.SuspendLayout();
			// 
			// tmrShowImage
			// 
			this.tmrShowImage.Interval = 1000;
			this.tmrShowImage.Tick += new System.EventHandler(this.tmrShowImage_Tick);
			// 
			// picScreen4
			// 
			this.picScreen4.BackColor = System.Drawing.Color.Black;
			this.picScreen4.Location = new System.Drawing.Point(36, 128);
			this.picScreen4.Name = "picScreen4";
			this.picScreen4.Size = new System.Drawing.Size(512, 128);
			this.picScreen4.TabIndex = 9;
			this.picScreen4.TabStop = false;
			// 
			// picScreen3
			// 
			this.picScreen3.BackColor = System.Drawing.Color.Black;
			this.picScreen3.Location = new System.Drawing.Point(36, 96);
			this.picScreen3.Name = "picScreen3";
			this.picScreen3.Size = new System.Drawing.Size(480, 32);
			this.picScreen3.TabIndex = 10;
			this.picScreen3.TabStop = false;
			// 
			// picScreen2
			// 
			this.picScreen2.BackColor = System.Drawing.Color.Black;
			this.picScreen2.Location = new System.Drawing.Point(36, 64);
			this.picScreen2.Name = "picScreen2";
			this.picScreen2.Size = new System.Drawing.Size(160, 32);
			this.picScreen2.TabIndex = 11;
			this.picScreen2.TabStop = false;
			// 
			// picScreen1
			// 
			this.picScreen1.BackColor = System.Drawing.Color.Black;
			this.picScreen1.Location = new System.Drawing.Point(36, 32);
			this.picScreen1.Name = "picScreen1";
			this.picScreen1.Size = new System.Drawing.Size(160, 32);
			this.picScreen1.TabIndex = 12;
			this.picScreen1.TabStop = false;
			// 
			// OutputForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(1120, 726);
			this.Controls.Add(this.picScreen4);
			this.Controls.Add(this.picScreen3);
			this.Controls.Add(this.picScreen2);
			this.Controls.Add(this.picScreen1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "OutputForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "MainForm";
			((System.ComponentModel.ISupportInitialize)(this.picScreen4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picScreen3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picScreen2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picScreen1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.FontDialog fontDialog1;
		private System.Windows.Forms.Timer tmrShowImage;
		private System.Windows.Forms.PictureBox picScreen4;
		private System.Windows.Forms.PictureBox picScreen3;
		private System.Windows.Forms.PictureBox picScreen2;
		private System.Windows.Forms.PictureBox picScreen1;
	}
}