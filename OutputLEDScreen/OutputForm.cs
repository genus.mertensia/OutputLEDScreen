﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.Structure;

namespace OutputLEDScreen
{

	public partial class OutputForm : Form
	{
		private SettingsInfo _settingsInfo;

		private static double factMargin = 0.95f;
		Thread scrollThread = null;
		Image<Bgr, Byte> imgScrollText;
		Image<Bgr, Byte> imgScrollShow;
		Color colorScrollBack = Color.Black;

		Thread videoThread = null;
		bool isVideoShowing;
		string[] imgFiles;
		int curImgIndex;

		#region Form Control

		public OutputForm()
		{
			isVideoShowing = false;
			imgFiles = new string[] { };
			curImgIndex = -1;

			InitializeComponent();
		}
		
		protected override void OnClosing(CancelEventArgs e)
		{
			if (scrollThread != null)
				scrollThread.Abort();

			if (videoThread != null)
				videoThread.Abort();

			base.OnClosing(e);
		}

		public void SetSettingsInfo(SettingsInfo s)
		{
			_settingsInfo = s;

			SetScreenControls();
		}

		private void SetScreenControls()
		{
			picScreen1.Location = _settingsInfo.AreaInfos[0].location;
			picScreen1.Size = _settingsInfo.AreaInfos[0].size;

			picScreen2.Location = _settingsInfo.AreaInfos[1].location;
			picScreen2.Size = _settingsInfo.AreaInfos[1].size;

			picScreen3.Location = _settingsInfo.AreaInfos[2].location;
			picScreen3.Size = _settingsInfo.AreaInfos[2].size;

			picScreen4.Location = _settingsInfo.AreaInfos[3].location;
			picScreen4.Size = _settingsInfo.AreaInfos[3].size;

			int maxWidth = 0, maxHeight = 0;

			for (int i = 0; i < _settingsInfo.AreaInfos.Length; i++)
			{
				if (maxWidth < _settingsInfo.AreaInfos[i].location.X + _settingsInfo.AreaInfos[i].size.Width)
					maxWidth = _settingsInfo.AreaInfos[i].location.X + _settingsInfo.AreaInfos[i].size.Width;

				if (maxHeight < _settingsInfo.AreaInfos[i].location.Y + _settingsInfo.AreaInfos[i].size.Height)
					maxHeight = _settingsInfo.AreaInfos[i].location.Y + _settingsInfo.AreaInfos[i].size.Height;
			}

			this.Width = maxWidth + 10;
			this.Height = maxHeight + 10;
			this.TopMost = true;
			this.Left = _settingsInfo.LEDOffset.X;
			//this.Width = 1024;
			//this.Height = 1280;

			string imgDir = _settingsInfo.ItemInfos[0].dirPath;

			tmrShowImage.Interval = 100;

			if (Directory.Exists(imgDir))
			{
				string supportedExtensions = "*.jpg,*.gif,*.png,*.bmp,*.jpe,*.jpeg,*.tif,*.tiff";

				imgFiles = Directory.GetFiles(imgDir, "*.*", SearchOption.AllDirectories).Where(s => supportedExtensions.Contains(Path.GetExtension(s).ToLower())).ToArray();

				curImgIndex = 0;
			}
			else
			{
				imgFiles = new string[] { };
				curImgIndex = -1;
			}

			tmrShowImage.Start();
		}

		#endregion

		#region Hotkey Video

		private string videoFile;
		private int maxPlayTime;

		public void SetVideo(int index)
		{
			videoFile = _settingsInfo.ItemInfos[index].dirPath;
			maxPlayTime = _settingsInfo.ItemInfos[index].displayTime;

			if (videoThread != null)
				videoThread.Abort();

			if (!File.Exists(videoFile))
			{
				videoFile = "";

				Graphics gr = picScreen4.CreateGraphics();
				Pen pen = new Pen(Color.Black, picScreen4.Width);
				gr.DrawRectangle(pen, new Rectangle(0, 0, picScreen4.Width, picScreen4.Height));
				gr.Dispose();
			}

			isVideoShowing = true;
			tmrShowImage.Interval = 100;

			videoThread = new Thread(ShowVideo);
			videoThread.Start();
		}

		private void ShowVideo()
		{
			VideoCapture cap = new VideoCapture(videoFile);

			if (!cap.IsOpened)
			{
				Debug.WriteLine("Load video file error");
				return;
			}

			//Get information about the video file
			double FrameRate = cap.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);
			double TotalFrames = cap.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
			cap.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, 0);

			if (TotalFrames > FrameRate * maxPlayTime)
				TotalFrames = FrameRate * maxPlayTime;

			double curFrames = 0;

			while (curFrames < TotalFrames)
			{
				picScreen4.Image = cap.QueryFrame().Bitmap;
								
				Application.DoEvents();

				Thread.Sleep((int)(1000.0 / FrameRate));

				curFrames = cap.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames);
			}

			cap.Dispose();

			isVideoShowing = false;
		}

		private void tmrShowImage_Tick(object sender, EventArgs e)
		{
			if (isVideoShowing)
				return;

			if (imgFiles.Length <= 0)
			{
				DrawBlankImageInPicturebox(picScreen4, Color.Black);

				tmrShowImage.Stop();
			}
			else
			{
				tmrShowImage.Interval = _settingsInfo.ItemInfos[0].displayTime * 1000;

				Image<Bgr, Byte> imgFile = new Image<Bgr, byte>(imgFiles[curImgIndex]);

				imgFile = imgFile.Resize(picScreen4.Width, picScreen4.Height, Emgu.CV.CvEnum.Inter.LinearExact);

				DrawImageInPicturebox(picScreen4, imgFile);

				curImgIndex++;

				if (curImgIndex >= imgFiles.Length)
					curImgIndex = 0;
			}
		}

		#endregion

		#region Display Image

		private void DrawBlankImageInPicturebox(PictureBox picBox, Color color)
		{
			try
			{
				Graphics gr = picBox.CreateGraphics();
				Pen pen = new Pen(color, picBox.Width);
				gr.DrawRectangle(pen, new Rectangle(0, 0, picBox.Width, picBox.Height));
				pen.Dispose();
				gr.Dispose();
			}
			catch { }
		}

		private void DrawImageInPicturebox(PictureBox picBox, Image<Bgr, Byte> img)
		{
			try
			{
				Graphics gr = picBox.CreateGraphics();
				Bitmap bmp = img.ToBitmap();
				gr.DrawImage(bmp, new Point(0, 0));
				bmp.Dispose();
				gr.Dispose();
			}
			catch { }
		}

		private void DrawImageIntoOtherImage(Image<Bgr, Byte> imgDst, Image<Bgr, Byte> imgOth)
		{
			// get intersection between rectangles
			Rectangle rect1 = new Rectangle(0, 0, imgDst.Width, imgDst.Height);
			Rectangle rect2 = new Rectangle(0, 0, imgOth.Width, imgOth.Height);

			Point pt1 = new Point((int)Math.Floor(imgDst.Width / 2.0), (int)Math.Floor(imgDst.Height / 2.0));
			Point pt2 = new Point((int)Math.Floor(imgOth.Width / 2.0), (int)Math.Floor(imgOth.Height / 2.0));

			rect2.Offset(pt1.X - pt2.X, pt1.Y - pt2.Y);

			Rectangle rectShowROI = rect1;
			Rectangle rectTextROI = rect2;

			rectShowROI.Intersect(rect2);
			rectTextROI.Intersect(rect1);
			rectTextROI.Offset(pt2.X - pt1.X, pt2.Y - pt1.Y);

			if (rectShowROI.Width > 0 && rectTextROI.Width > 0)
			{
				imgDst.ROI = rectShowROI;
				imgOth.ROI = rectTextROI;
				imgOth.CopyTo(imgDst);
			}

			imgDst.ROI = Rectangle.Empty;
			imgOth.ROI = Rectangle.Empty;
		}

		public void SetStaticImage(RichTextBox rtbRealMsg, int index)
		{
			PictureBox picScreen = index == 1 ? picScreen1 : picScreen2;

			Bitmap bmp = DrawRtfText.DrawToBitmap(rtbRealMsg);

			Image<Bgr, Byte> imgStaticText = new Image<Bgr, byte>(bmp);
			Image<Bgr, Byte> imgStaticShow = new Image<Bgr, byte>(picScreen.Width, picScreen.Height, new Bgr(Color.Black));

			if (rtbRealMsg.Text.Length == 0)
			{
				picScreen.Image = imgStaticShow.ToBitmap();
				return;
			}

			imgStaticShow.Draw(new Rectangle(0, 0, imgStaticShow.Width, imgStaticShow.Height), new Bgr(rtbRealMsg.BackColor), -1);

			/*if (imgStaticText.Height > (imgStaticShow.Height * factMargin))
			{
				double fact = (imgStaticShow.Height * factMargin) / imgStaticText.Height;
				imgStaticText = imgStaticText.Resize(fact, Emgu.CV.CvEnum.Inter.LinearExact);
			}*/

			if (imgStaticText.Width > (imgStaticShow.Width * factMargin))
			{
				double fact = (imgStaticShow.Width * factMargin) / imgStaticText.Width;
				imgStaticText = imgStaticText.Resize(fact, Emgu.CV.CvEnum.Inter.LinearExact);
			}

			DrawImageIntoOtherImage(imgStaticShow, imgStaticText);

			//DrawImageInPicturebox(picScreen, imgStaticShow);
			picScreen.Image = imgStaticShow.ToBitmap();
		}

		public void SetScrollMsg(RichTextBox rtbRealMsg)
		{
			if (scrollThread != null)
				scrollThread.Abort();

			Bitmap bmp = DrawRtfText.DrawToBitmap(rtbRealMsg);
			colorScrollBack = rtbRealMsg.BackColor;

			imgScrollText = new Image<Bgr, byte>(bmp);
			imgScrollShow = new Image<Bgr, byte>(picScreen3.Width, picScreen3.Height, new Bgr(Color.Black));

			if (rtbRealMsg.Text.Length == 0)
			{
				picScreen3.Image = imgScrollShow.ToBitmap();
				return;
			}

			if (imgScrollText.Height > (imgScrollShow.Height * factMargin))
			{
				double fact = (imgScrollShow.Height * factMargin) / imgScrollText.Height;
				imgScrollText = imgScrollText.Resize(fact, Emgu.CV.CvEnum.Inter.LinearExact);
			}

			scrollThread = new Thread(ShowScrollMsg);
			scrollThread.Start();
		}

		public float scrollNextDistRate = 0.7f;

		private void ShowScrollMsg()
		{
			float[] fx = { 0, 0 };
			fx[0] = imgScrollShow.Width;
			fx[1] = fx[0] + imgScrollText.Width + (int)(imgScrollShow.Width * scrollNextDistRate);

			while (true)
			{
				//Stopwatch sw = new Stopwatch();
				//sw.Start();

				imgScrollShow.Draw(new Rectangle(0, 0, imgScrollShow.Width, imgScrollShow.Height), new Bgr(colorScrollBack), -1);

				if (fx[0] < -imgScrollText.Width - 10)
					fx[0] = fx[1] + imgScrollText.Width + (int)(imgScrollShow.Width * scrollNextDistRate);

				if (fx[1] < -imgScrollText.Width - 10)
					fx[1] = fx[0] + imgScrollText.Width + (int)(imgScrollShow.Width * scrollNextDistRate);

				for (int i = 0; i < 2; i++)
				{
					int x = (int)fx[i];

					/*Rectangle rectShowROI = new Rectangle(Math.Max(x, 0),
												(imgScrollShow.Height - imgScrollText.Height) / 2,
												Math.Min(imgScrollShow.Width - x, imgScrollText.Width + Math.Min(x, 0)),
												imgScrollText.Height);
					Rectangle rectTextROI = new Rectangle(-Math.Min(x, 0),
												0,
												Math.Min(imgScrollShow.Width - x, imgScrollText.Width - Math.Min(x, 0)),
												imgScrollText.Height);*/

					Rectangle rect1 = new Rectangle(0, 0, imgScrollShow.Width, imgScrollShow.Height);
					Rectangle rect2 = new Rectangle(0, 0, imgScrollText.Width, imgScrollText.Height);

					rect2.Offset(x, (imgScrollShow.Height - imgScrollText.Height) / 2);

					Rectangle rectShowROI = rect1;
					Rectangle rectTextROI = rect2;

					rectShowROI.Intersect(rect2);
					rectTextROI.Intersect(rect1);
					rectTextROI.Offset(-x, -(imgScrollShow.Height - imgScrollText.Height) / 2);

					if (rectShowROI.Width > 0 && rectTextROI.Width > 0)
					{
						imgScrollShow.ROI = rectShowROI;
						imgScrollText.ROI = rectTextROI;
						imgScrollText.CopyTo(imgScrollShow);
					}

					imgScrollShow.ROI = Rectangle.Empty;
					imgScrollText.ROI = Rectangle.Empty;
				}

				DrawImageInPicturebox(picScreen3, imgScrollShow);

				fx[0] -= _settingsInfo.TextInfos[2].scrollSpeed / 7.0f;
				fx[1] -= _settingsInfo.TextInfos[2].scrollSpeed / 7.0f;

				//sw.Stop();

				Application.DoEvents();

				Thread.Sleep(10);
			}
		}

		#endregion
	}
}
