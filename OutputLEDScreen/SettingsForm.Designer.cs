﻿namespace OutputLEDScreen
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.nudTimeSponsor = new System.Windows.Forms.NumericUpDown();
			this.txtFileSponsor = new System.Windows.Forms.TextBox();
			this.btnBrowseSponsor = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.toolStrip2 = new System.Windows.Forms.ToolStrip();
			this.btnBackColorPickVideo1 = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.btnForeColorPickVideo1 = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.nudTimeVideo1 = new System.Windows.Forms.NumericUpDown();
			this.txtNameVideo1 = new System.Windows.Forms.TextBox();
			this.txtFileVideo1 = new System.Windows.Forms.TextBox();
			this.btnBrowseVideo1 = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.toolStrip3 = new System.Windows.Forms.ToolStrip();
			this.btnBackColorPickVideo2 = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.btnForeColorPickVideo2 = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.nudTimeVideo2 = new System.Windows.Forms.NumericUpDown();
			this.txtNameVideo2 = new System.Windows.Forms.TextBox();
			this.txtFileVideo2 = new System.Windows.Forms.TextBox();
			this.btnBrowseVideo2 = new System.Windows.Forms.Button();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.toolStrip4 = new System.Windows.Forms.ToolStrip();
			this.btnBackColorPickVideo3 = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.btnForeColorPickVideo3 = new Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton();
			this.nudTimeVideo3 = new System.Windows.Forms.NumericUpDown();
			this.txtNameVideo3 = new System.Windows.Forms.TextBox();
			this.txtFileVideo3 = new System.Windows.Forms.TextBox();
			this.btnBrowseVideo3 = new System.Windows.Forms.Button();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.btnAreaSave = new System.Windows.Forms.Button();
			this.nudVideoSizeH = new System.Windows.Forms.NumericUpDown();
			this.nudMsgSizeH = new System.Windows.Forms.NumericUpDown();
			this.nudAwaySizeH = new System.Windows.Forms.NumericUpDown();
			this.nudHomeSizeH = new System.Windows.Forms.NumericUpDown();
			this.nudVideoSizeW = new System.Windows.Forms.NumericUpDown();
			this.nudMsgSizeW = new System.Windows.Forms.NumericUpDown();
			this.nudAwaySizeW = new System.Windows.Forms.NumericUpDown();
			this.nudHomeSizeW = new System.Windows.Forms.NumericUpDown();
			this.nudMsgLocY = new System.Windows.Forms.NumericUpDown();
			this.nudAwayLocY = new System.Windows.Forms.NumericUpDown();
			this.nudHomeLocY = new System.Windows.Forms.NumericUpDown();
			this.nudVideoLocX = new System.Windows.Forms.NumericUpDown();
			this.nudMsgLocX = new System.Windows.Forms.NumericUpDown();
			this.nudAwayLocX = new System.Windows.Forms.NumericUpDown();
			this.nudHomeLocX = new System.Windows.Forms.NumericUpDown();
			this.label35 = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label33 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label32 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.nudLedOffsetX = new System.Windows.Forms.NumericUpDown();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnAllSave = new System.Windows.Forms.Button();
			this.label36 = new System.Windows.Forms.Label();
			this.nudVideoLocY = new System.Windows.Forms.NumericUpDown();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeSponsor)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.toolStrip2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeVideo1)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.toolStrip3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeVideo2)).BeginInit();
			this.groupBox4.SuspendLayout();
			this.toolStrip4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeVideo3)).BeginInit();
			this.groupBox5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudVideoSizeH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudMsgSizeH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudAwaySizeH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHomeSizeH)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudVideoSizeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudMsgSizeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudAwaySizeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHomeSizeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudMsgLocY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudAwayLocY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHomeLocY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudVideoLocX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudMsgLocX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudAwayLocX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHomeLocX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudLedOffsetX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudVideoLocY)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.nudTimeSponsor);
			this.groupBox1.Controls.Add(this.txtFileSponsor);
			this.groupBox1.Controls.Add(this.btnBrowseSponsor);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(753, 106);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			// 
			// nudTimeSponsor
			// 
			this.nudTimeSponsor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudTimeSponsor.Location = new System.Drawing.Point(487, 16);
			this.nudTimeSponsor.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudTimeSponsor.Name = "nudTimeSponsor";
			this.nudTimeSponsor.Size = new System.Drawing.Size(66, 31);
			this.nudTimeSponsor.TabIndex = 4;
			this.nudTimeSponsor.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
			// 
			// txtFileSponsor
			// 
			this.txtFileSponsor.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtFileSponsor.Location = new System.Drawing.Point(11, 53);
			this.txtFileSponsor.Name = "txtFileSponsor";
			this.txtFileSponsor.ReadOnly = true;
			this.txtFileSponsor.Size = new System.Drawing.Size(343, 38);
			this.txtFileSponsor.TabIndex = 3;
			this.txtFileSponsor.Text = "Image";
			// 
			// btnBrowseSponsor
			// 
			this.btnBrowseSponsor.BackgroundImage = global::OutputLEDScreen.Properties.Resources.folder_icons1;
			this.btnBrowseSponsor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnBrowseSponsor.FlatAppearance.BorderSize = 0;
			this.btnBrowseSponsor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBrowseSponsor.Location = new System.Drawing.Point(317, 16);
			this.btnBrowseSponsor.Name = "btnBrowseSponsor";
			this.btnBrowseSponsor.Size = new System.Drawing.Size(37, 31);
			this.btnBrowseSponsor.TabIndex = 2;
			this.btnBrowseSponsor.UseVisualStyleBackColor = true;
			this.btnBrowseSponsor.Click += new System.EventHandler(this.btnBrowseSponsor_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(559, 21);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 20);
			this.label3.TabIndex = 1;
			this.label3.Text = "Seconds";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(383, 21);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(98, 20);
			this.label2.TabIndex = 1;
			this.label2.Text = "Display Time";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(6, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(248, 25);
			this.label1.TabIndex = 1;
			this.label1.Text = "Sponser Image Directory";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.toolStrip2);
			this.groupBox2.Controls.Add(this.nudTimeVideo1);
			this.groupBox2.Controls.Add(this.txtNameVideo1);
			this.groupBox2.Controls.Add(this.txtFileVideo1);
			this.groupBox2.Controls.Add(this.btnBrowseVideo1);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Location = new System.Drawing.Point(12, 124);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(753, 106);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			// 
			// toolStrip2
			// 
			this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBackColorPickVideo1,
            this.btnForeColorPickVideo1});
			this.toolStrip2.Location = new System.Drawing.Point(664, 60);
			this.toolStrip2.Name = "toolStrip2";
			this.toolStrip2.Padding = new System.Windows.Forms.Padding(0);
			this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip2.Size = new System.Drawing.Size(82, 31);
			this.toolStrip2.TabIndex = 3;
			// 
			// btnBackColorPickVideo1
			// 
			this.btnBackColorPickVideo1.Color = System.Drawing.Color.Yellow;
			this.btnBackColorPickVideo1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBackColorPickVideo1.Image = ((System.Drawing.Image)(resources.GetObject("btnBackColorPickVideo1.Image")));
			this.btnBackColorPickVideo1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBackColorPickVideo1.Name = "btnBackColorPickVideo1";
			this.btnBackColorPickVideo1.Size = new System.Drawing.Size(40, 28);
			this.btnBackColorPickVideo1.Text = "toolStripColorPickerSplitButton1";
			this.btnBackColorPickVideo1.ColorChanged += new System.EventHandler(this.btnBackColorPickVideo1_ColorChanged);
			// 
			// btnForeColorPickVideo1
			// 
			this.btnForeColorPickVideo1.Color = System.Drawing.Color.Red;
			this.btnForeColorPickVideo1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnForeColorPickVideo1.Image = ((System.Drawing.Image)(resources.GetObject("btnForeColorPickVideo1.Image")));
			this.btnForeColorPickVideo1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnForeColorPickVideo1.Name = "btnForeColorPickVideo1";
			this.btnForeColorPickVideo1.Size = new System.Drawing.Size(40, 28);
			this.btnForeColorPickVideo1.Text = "toolStripColorPickerSplitButton2";
			this.btnForeColorPickVideo1.ColorChanged += new System.EventHandler(this.btnForeColorPickVideo1_ColorChanged);
			// 
			// nudTimeVideo1
			// 
			this.nudTimeVideo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudTimeVideo1.Location = new System.Drawing.Point(487, 16);
			this.nudTimeVideo1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudTimeVideo1.Name = "nudTimeVideo1";
			this.nudTimeVideo1.Size = new System.Drawing.Size(66, 31);
			this.nudTimeVideo1.TabIndex = 4;
			this.nudTimeVideo1.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
			// 
			// txtNameVideo1
			// 
			this.txtNameVideo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtNameVideo1.Location = new System.Drawing.Point(535, 60);
			this.txtNameVideo1.Name = "txtNameVideo1";
			this.txtNameVideo1.Size = new System.Drawing.Size(124, 31);
			this.txtNameVideo1.TabIndex = 3;
			this.txtNameVideo1.Text = "Video1";
			this.txtNameVideo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// txtFileVideo1
			// 
			this.txtFileVideo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtFileVideo1.Location = new System.Drawing.Point(11, 53);
			this.txtFileVideo1.Name = "txtFileVideo1";
			this.txtFileVideo1.ReadOnly = true;
			this.txtFileVideo1.Size = new System.Drawing.Size(343, 38);
			this.txtFileVideo1.TabIndex = 3;
			this.txtFileVideo1.Text = "video1.mp4";
			// 
			// btnBrowseVideo1
			// 
			this.btnBrowseVideo1.BackgroundImage = global::OutputLEDScreen.Properties.Resources.folder_icons1;
			this.btnBrowseVideo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnBrowseVideo1.FlatAppearance.BorderSize = 0;
			this.btnBrowseVideo1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBrowseVideo1.Location = new System.Drawing.Point(317, 16);
			this.btnBrowseVideo1.Name = "btnBrowseVideo1";
			this.btnBrowseVideo1.Size = new System.Drawing.Size(37, 31);
			this.btnBrowseVideo1.TabIndex = 2;
			this.btnBrowseVideo1.UseVisualStyleBackColor = true;
			this.btnBrowseVideo1.Click += new System.EventHandler(this.btnBrowseVideo1_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(559, 21);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 20);
			this.label5.TabIndex = 1;
			this.label5.Text = "Seconds";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(371, 66);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(158, 20);
			this.label6.TabIndex = 1;
			this.label6.Text = "Button Display Name";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(383, 21);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(98, 20);
			this.label7.TabIndex = 1;
			this.label7.Text = "Display Time";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(6, 16);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(236, 25);
			this.label8.TabIndex = 1;
			this.label8.Text = "Video1 Hotkey Settings";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.toolStrip3);
			this.groupBox3.Controls.Add(this.nudTimeVideo2);
			this.groupBox3.Controls.Add(this.txtNameVideo2);
			this.groupBox3.Controls.Add(this.txtFileVideo2);
			this.groupBox3.Controls.Add(this.btnBrowseVideo2);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.label11);
			this.groupBox3.Controls.Add(this.label12);
			this.groupBox3.Location = new System.Drawing.Point(12, 236);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(753, 106);
			this.groupBox3.TabIndex = 1;
			this.groupBox3.TabStop = false;
			// 
			// toolStrip3
			// 
			this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip3.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBackColorPickVideo2,
            this.btnForeColorPickVideo2});
			this.toolStrip3.Location = new System.Drawing.Point(664, 60);
			this.toolStrip3.Name = "toolStrip3";
			this.toolStrip3.Padding = new System.Windows.Forms.Padding(0);
			this.toolStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip3.Size = new System.Drawing.Size(82, 31);
			this.toolStrip3.TabIndex = 3;
			// 
			// btnBackColorPickVideo2
			// 
			this.btnBackColorPickVideo2.Color = System.Drawing.Color.Yellow;
			this.btnBackColorPickVideo2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBackColorPickVideo2.Image = ((System.Drawing.Image)(resources.GetObject("btnBackColorPickVideo2.Image")));
			this.btnBackColorPickVideo2.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBackColorPickVideo2.Name = "btnBackColorPickVideo2";
			this.btnBackColorPickVideo2.Size = new System.Drawing.Size(40, 28);
			this.btnBackColorPickVideo2.Text = "toolStripColorPickerSplitButton1";
			this.btnBackColorPickVideo2.ColorChanged += new System.EventHandler(this.btnBackColorPickVideo2_ColorChanged);
			// 
			// btnForeColorPickVideo2
			// 
			this.btnForeColorPickVideo2.Color = System.Drawing.Color.Red;
			this.btnForeColorPickVideo2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnForeColorPickVideo2.Image = ((System.Drawing.Image)(resources.GetObject("btnForeColorPickVideo2.Image")));
			this.btnForeColorPickVideo2.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnForeColorPickVideo2.Name = "btnForeColorPickVideo2";
			this.btnForeColorPickVideo2.Size = new System.Drawing.Size(40, 28);
			this.btnForeColorPickVideo2.Text = "toolStripColorPickerSplitButton2";
			this.btnForeColorPickVideo2.ColorChanged += new System.EventHandler(this.btnForeColorPickVideo2_ColorChanged);
			// 
			// nudTimeVideo2
			// 
			this.nudTimeVideo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudTimeVideo2.Location = new System.Drawing.Point(487, 16);
			this.nudTimeVideo2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudTimeVideo2.Name = "nudTimeVideo2";
			this.nudTimeVideo2.Size = new System.Drawing.Size(66, 31);
			this.nudTimeVideo2.TabIndex = 4;
			this.nudTimeVideo2.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
			// 
			// txtNameVideo2
			// 
			this.txtNameVideo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtNameVideo2.Location = new System.Drawing.Point(535, 60);
			this.txtNameVideo2.Name = "txtNameVideo2";
			this.txtNameVideo2.Size = new System.Drawing.Size(124, 31);
			this.txtNameVideo2.TabIndex = 3;
			this.txtNameVideo2.Text = "Video2";
			this.txtNameVideo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// txtFileVideo2
			// 
			this.txtFileVideo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtFileVideo2.Location = new System.Drawing.Point(11, 53);
			this.txtFileVideo2.Name = "txtFileVideo2";
			this.txtFileVideo2.ReadOnly = true;
			this.txtFileVideo2.Size = new System.Drawing.Size(343, 38);
			this.txtFileVideo2.TabIndex = 3;
			this.txtFileVideo2.Text = "video2.mp4";
			// 
			// btnBrowseVideo2
			// 
			this.btnBrowseVideo2.BackgroundImage = global::OutputLEDScreen.Properties.Resources.folder_icons1;
			this.btnBrowseVideo2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnBrowseVideo2.FlatAppearance.BorderSize = 0;
			this.btnBrowseVideo2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBrowseVideo2.Location = new System.Drawing.Point(317, 16);
			this.btnBrowseVideo2.Name = "btnBrowseVideo2";
			this.btnBrowseVideo2.Size = new System.Drawing.Size(37, 31);
			this.btnBrowseVideo2.TabIndex = 2;
			this.btnBrowseVideo2.UseVisualStyleBackColor = true;
			this.btnBrowseVideo2.Click += new System.EventHandler(this.btnBrowseVideo2_Click);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(559, 21);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(72, 20);
			this.label9.TabIndex = 1;
			this.label9.Text = "Seconds";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(371, 66);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(158, 20);
			this.label10.TabIndex = 1;
			this.label10.Text = "Button Display Name";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(383, 21);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(98, 20);
			this.label11.TabIndex = 1;
			this.label11.Text = "Display Time";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(6, 16);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(236, 25);
			this.label12.TabIndex = 1;
			this.label12.Text = "Video2 Hotkey Settings";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.toolStrip4);
			this.groupBox4.Controls.Add(this.nudTimeVideo3);
			this.groupBox4.Controls.Add(this.txtNameVideo3);
			this.groupBox4.Controls.Add(this.txtFileVideo3);
			this.groupBox4.Controls.Add(this.btnBrowseVideo3);
			this.groupBox4.Controls.Add(this.label13);
			this.groupBox4.Controls.Add(this.label14);
			this.groupBox4.Controls.Add(this.label15);
			this.groupBox4.Controls.Add(this.label16);
			this.groupBox4.Location = new System.Drawing.Point(12, 348);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(753, 106);
			this.groupBox4.TabIndex = 1;
			this.groupBox4.TabStop = false;
			// 
			// toolStrip4
			// 
			this.toolStrip4.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip4.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBackColorPickVideo3,
            this.btnForeColorPickVideo3});
			this.toolStrip4.Location = new System.Drawing.Point(664, 60);
			this.toolStrip4.Name = "toolStrip4";
			this.toolStrip4.Padding = new System.Windows.Forms.Padding(0);
			this.toolStrip4.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.toolStrip4.Size = new System.Drawing.Size(82, 31);
			this.toolStrip4.TabIndex = 3;
			// 
			// btnBackColorPickVideo3
			// 
			this.btnBackColorPickVideo3.Color = System.Drawing.Color.Yellow;
			this.btnBackColorPickVideo3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnBackColorPickVideo3.Image = ((System.Drawing.Image)(resources.GetObject("btnBackColorPickVideo3.Image")));
			this.btnBackColorPickVideo3.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnBackColorPickVideo3.Name = "btnBackColorPickVideo3";
			this.btnBackColorPickVideo3.Size = new System.Drawing.Size(40, 28);
			this.btnBackColorPickVideo3.Text = "toolStripColorPickerSplitButton1";
			this.btnBackColorPickVideo3.ColorChanged += new System.EventHandler(this.btnBackColorPickVideo3_ColorChanged);
			// 
			// btnForeColorPickVideo3
			// 
			this.btnForeColorPickVideo3.Color = System.Drawing.Color.Red;
			this.btnForeColorPickVideo3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnForeColorPickVideo3.Image = ((System.Drawing.Image)(resources.GetObject("btnForeColorPickVideo3.Image")));
			this.btnForeColorPickVideo3.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnForeColorPickVideo3.Name = "btnForeColorPickVideo3";
			this.btnForeColorPickVideo3.Size = new System.Drawing.Size(40, 28);
			this.btnForeColorPickVideo3.Text = "toolStripColorPickerSplitButton2";
			this.btnForeColorPickVideo3.ColorChanged += new System.EventHandler(this.btnForeColorPickVideo3_ColorChanged);
			// 
			// nudTimeVideo3
			// 
			this.nudTimeVideo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudTimeVideo3.Location = new System.Drawing.Point(487, 16);
			this.nudTimeVideo3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudTimeVideo3.Name = "nudTimeVideo3";
			this.nudTimeVideo3.Size = new System.Drawing.Size(66, 31);
			this.nudTimeVideo3.TabIndex = 4;
			this.nudTimeVideo3.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
			// 
			// txtNameVideo3
			// 
			this.txtNameVideo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtNameVideo3.Location = new System.Drawing.Point(535, 60);
			this.txtNameVideo3.Name = "txtNameVideo3";
			this.txtNameVideo3.Size = new System.Drawing.Size(124, 31);
			this.txtNameVideo3.TabIndex = 3;
			this.txtNameVideo3.Text = "Video3";
			this.txtNameVideo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// txtFileVideo3
			// 
			this.txtFileVideo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtFileVideo3.Location = new System.Drawing.Point(11, 53);
			this.txtFileVideo3.Name = "txtFileVideo3";
			this.txtFileVideo3.ReadOnly = true;
			this.txtFileVideo3.Size = new System.Drawing.Size(343, 38);
			this.txtFileVideo3.TabIndex = 3;
			this.txtFileVideo3.Text = "video3.mp4";
			// 
			// btnBrowseVideo3
			// 
			this.btnBrowseVideo3.BackgroundImage = global::OutputLEDScreen.Properties.Resources.folder_icons1;
			this.btnBrowseVideo3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnBrowseVideo3.FlatAppearance.BorderSize = 0;
			this.btnBrowseVideo3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBrowseVideo3.Location = new System.Drawing.Point(317, 16);
			this.btnBrowseVideo3.Name = "btnBrowseVideo3";
			this.btnBrowseVideo3.Size = new System.Drawing.Size(37, 31);
			this.btnBrowseVideo3.TabIndex = 2;
			this.btnBrowseVideo3.UseVisualStyleBackColor = true;
			this.btnBrowseVideo3.Click += new System.EventHandler(this.btnBrowseVideo3_Click);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.Location = new System.Drawing.Point(559, 21);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(72, 20);
			this.label13.TabIndex = 1;
			this.label13.Text = "Seconds";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.Location = new System.Drawing.Point(371, 66);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(158, 20);
			this.label14.TabIndex = 1;
			this.label14.Text = "Button Display Name";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(383, 21);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(98, 20);
			this.label15.TabIndex = 1;
			this.label15.Text = "Display Time";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(6, 16);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(236, 25);
			this.label16.TabIndex = 1;
			this.label16.Text = "Video3 Hotkey Settings";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.btnAreaSave);
			this.groupBox5.Controls.Add(this.nudVideoSizeH);
			this.groupBox5.Controls.Add(this.nudMsgSizeH);
			this.groupBox5.Controls.Add(this.nudAwaySizeH);
			this.groupBox5.Controls.Add(this.nudHomeSizeH);
			this.groupBox5.Controls.Add(this.nudVideoSizeW);
			this.groupBox5.Controls.Add(this.nudMsgSizeW);
			this.groupBox5.Controls.Add(this.nudAwaySizeW);
			this.groupBox5.Controls.Add(this.nudHomeSizeW);
			this.groupBox5.Controls.Add(this.nudVideoLocY);
			this.groupBox5.Controls.Add(this.nudMsgLocY);
			this.groupBox5.Controls.Add(this.nudAwayLocY);
			this.groupBox5.Controls.Add(this.nudHomeLocY);
			this.groupBox5.Controls.Add(this.nudVideoLocX);
			this.groupBox5.Controls.Add(this.nudMsgLocX);
			this.groupBox5.Controls.Add(this.nudAwayLocX);
			this.groupBox5.Controls.Add(this.nudHomeLocX);
			this.groupBox5.Controls.Add(this.label35);
			this.groupBox5.Controls.Add(this.label34);
			this.groupBox5.Controls.Add(this.label30);
			this.groupBox5.Controls.Add(this.label29);
			this.groupBox5.Controls.Add(this.label25);
			this.groupBox5.Controls.Add(this.label33);
			this.groupBox5.Controls.Add(this.label24);
			this.groupBox5.Controls.Add(this.label28);
			this.groupBox5.Controls.Add(this.label4);
			this.groupBox5.Controls.Add(this.label32);
			this.groupBox5.Controls.Add(this.label23);
			this.groupBox5.Controls.Add(this.label27);
			this.groupBox5.Controls.Add(this.label20);
			this.groupBox5.Controls.Add(this.label31);
			this.groupBox5.Controls.Add(this.label22);
			this.groupBox5.Controls.Add(this.label26);
			this.groupBox5.Controls.Add(this.label18);
			this.groupBox5.Controls.Add(this.label21);
			this.groupBox5.Controls.Add(this.label19);
			this.groupBox5.Controls.Add(this.label17);
			this.groupBox5.Location = new System.Drawing.Point(771, 12);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(263, 382);
			this.groupBox5.TabIndex = 2;
			this.groupBox5.TabStop = false;
			// 
			// btnAreaSave
			// 
			this.btnAreaSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAreaSave.Location = new System.Drawing.Point(68, 335);
			this.btnAreaSave.Name = "btnAreaSave";
			this.btnAreaSave.Size = new System.Drawing.Size(148, 36);
			this.btnAreaSave.TabIndex = 5;
			this.btnAreaSave.Text = "Enable Editing";
			this.btnAreaSave.UseVisualStyleBackColor = true;
			this.btnAreaSave.Click += new System.EventHandler(this.btnSizeSave_Click);
			// 
			// nudVideoSizeH
			// 
			this.nudVideoSizeH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudVideoSizeH.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudVideoSizeH.Location = new System.Drawing.Point(176, 302);
			this.nudVideoSizeH.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudVideoSizeH.Name = "nudVideoSizeH";
			this.nudVideoSizeH.Size = new System.Drawing.Size(59, 22);
			this.nudVideoSizeH.TabIndex = 4;
			this.nudVideoSizeH.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudMsgSizeH
			// 
			this.nudMsgSizeH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudMsgSizeH.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudMsgSizeH.Location = new System.Drawing.Point(176, 224);
			this.nudMsgSizeH.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudMsgSizeH.Name = "nudMsgSizeH";
			this.nudMsgSizeH.Size = new System.Drawing.Size(59, 22);
			this.nudMsgSizeH.TabIndex = 4;
			this.nudMsgSizeH.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudAwaySizeH
			// 
			this.nudAwaySizeH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudAwaySizeH.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudAwaySizeH.Location = new System.Drawing.Point(176, 146);
			this.nudAwaySizeH.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudAwaySizeH.Name = "nudAwaySizeH";
			this.nudAwaySizeH.Size = new System.Drawing.Size(59, 22);
			this.nudAwaySizeH.TabIndex = 4;
			this.nudAwaySizeH.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudHomeSizeH
			// 
			this.nudHomeSizeH.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudHomeSizeH.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudHomeSizeH.Location = new System.Drawing.Point(176, 68);
			this.nudHomeSizeH.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudHomeSizeH.Name = "nudHomeSizeH";
			this.nudHomeSizeH.Size = new System.Drawing.Size(59, 22);
			this.nudHomeSizeH.TabIndex = 4;
			this.nudHomeSizeH.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudVideoSizeW
			// 
			this.nudVideoSizeW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudVideoSizeW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudVideoSizeW.Location = new System.Drawing.Point(87, 302);
			this.nudVideoSizeW.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudVideoSizeW.Name = "nudVideoSizeW";
			this.nudVideoSizeW.Size = new System.Drawing.Size(59, 22);
			this.nudVideoSizeW.TabIndex = 4;
			this.nudVideoSizeW.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudMsgSizeW
			// 
			this.nudMsgSizeW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudMsgSizeW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudMsgSizeW.Location = new System.Drawing.Point(87, 224);
			this.nudMsgSizeW.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudMsgSizeW.Name = "nudMsgSizeW";
			this.nudMsgSizeW.Size = new System.Drawing.Size(59, 22);
			this.nudMsgSizeW.TabIndex = 4;
			this.nudMsgSizeW.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudAwaySizeW
			// 
			this.nudAwaySizeW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudAwaySizeW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudAwaySizeW.Location = new System.Drawing.Point(87, 146);
			this.nudAwaySizeW.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudAwaySizeW.Name = "nudAwaySizeW";
			this.nudAwaySizeW.Size = new System.Drawing.Size(59, 22);
			this.nudAwaySizeW.TabIndex = 4;
			this.nudAwaySizeW.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudHomeSizeW
			// 
			this.nudHomeSizeW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudHomeSizeW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudHomeSizeW.Location = new System.Drawing.Point(87, 68);
			this.nudHomeSizeW.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudHomeSizeW.Name = "nudHomeSizeW";
			this.nudHomeSizeW.Size = new System.Drawing.Size(59, 22);
			this.nudHomeSizeW.TabIndex = 4;
			this.nudHomeSizeW.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudMsgLocY
			// 
			this.nudMsgLocY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudMsgLocY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudMsgLocY.Location = new System.Drawing.Point(176, 199);
			this.nudMsgLocY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudMsgLocY.Name = "nudMsgLocY";
			this.nudMsgLocY.Size = new System.Drawing.Size(59, 22);
			this.nudMsgLocY.TabIndex = 4;
			this.nudMsgLocY.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudAwayLocY
			// 
			this.nudAwayLocY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudAwayLocY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudAwayLocY.Location = new System.Drawing.Point(176, 121);
			this.nudAwayLocY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudAwayLocY.Name = "nudAwayLocY";
			this.nudAwayLocY.Size = new System.Drawing.Size(59, 22);
			this.nudAwayLocY.TabIndex = 4;
			this.nudAwayLocY.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudHomeLocY
			// 
			this.nudHomeLocY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudHomeLocY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudHomeLocY.Location = new System.Drawing.Point(176, 43);
			this.nudHomeLocY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudHomeLocY.Name = "nudHomeLocY";
			this.nudHomeLocY.Size = new System.Drawing.Size(59, 22);
			this.nudHomeLocY.TabIndex = 4;
			this.nudHomeLocY.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudVideoLocX
			// 
			this.nudVideoLocX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudVideoLocX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudVideoLocX.Location = new System.Drawing.Point(87, 277);
			this.nudVideoLocX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudVideoLocX.Name = "nudVideoLocX";
			this.nudVideoLocX.Size = new System.Drawing.Size(59, 22);
			this.nudVideoLocX.TabIndex = 4;
			this.nudVideoLocX.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudMsgLocX
			// 
			this.nudMsgLocX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudMsgLocX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudMsgLocX.Location = new System.Drawing.Point(87, 199);
			this.nudMsgLocX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudMsgLocX.Name = "nudMsgLocX";
			this.nudMsgLocX.Size = new System.Drawing.Size(59, 22);
			this.nudMsgLocX.TabIndex = 4;
			this.nudMsgLocX.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudAwayLocX
			// 
			this.nudAwayLocX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudAwayLocX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudAwayLocX.Location = new System.Drawing.Point(87, 121);
			this.nudAwayLocX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudAwayLocX.Name = "nudAwayLocX";
			this.nudAwayLocX.Size = new System.Drawing.Size(59, 22);
			this.nudAwayLocX.TabIndex = 4;
			this.nudAwayLocX.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// nudHomeLocX
			// 
			this.nudHomeLocX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudHomeLocX.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudHomeLocX.Location = new System.Drawing.Point(87, 43);
			this.nudHomeLocX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudHomeLocX.Name = "nudHomeLocX";
			this.nudHomeLocX.Size = new System.Drawing.Size(59, 22);
			this.nudHomeLocX.TabIndex = 4;
			this.nudHomeLocX.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// label35
			// 
			this.label35.AutoSize = true;
			this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label35.Location = new System.Drawing.Point(22, 253);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(196, 20);
			this.label35.TabIndex = 1;
			this.label35.Text = "Picture/Video Display Area";
			// 
			// label34
			// 
			this.label34.AutoSize = true;
			this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label34.Location = new System.Drawing.Point(151, 303);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(18, 20);
			this.label34.TabIndex = 1;
			this.label34.Text = "×";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label30.Location = new System.Drawing.Point(22, 175);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(196, 20);
			this.label30.TabIndex = 1;
			this.label30.Text = "Scrolling Msg Display Area";
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label29.Location = new System.Drawing.Point(151, 225);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(18, 20);
			this.label29.TabIndex = 1;
			this.label29.Text = "×";
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label25.Location = new System.Drawing.Point(21, 97);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(184, 20);
			this.label25.TabIndex = 1;
			this.label25.Text = "Away Team Display Area";
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label33.Location = new System.Drawing.Point(28, 305);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(31, 15);
			this.label33.TabIndex = 1;
			this.label33.Text = "Size";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label24.Location = new System.Drawing.Point(151, 147);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(18, 20);
			this.label24.TabIndex = 1;
			this.label24.Text = "×";
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label28.Location = new System.Drawing.Point(28, 227);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(31, 15);
			this.label28.TabIndex = 1;
			this.label28.Text = "Size";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(22, 19);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(189, 20);
			this.label4.TabIndex = 1;
			this.label4.Text = "Home Team Display Area";
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label32.Location = new System.Drawing.Point(151, 278);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(18, 20);
			this.label32.TabIndex = 1;
			this.label32.Text = "×";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label23.Location = new System.Drawing.Point(28, 149);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(31, 15);
			this.label23.TabIndex = 1;
			this.label23.Text = "Size";
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label27.Location = new System.Drawing.Point(151, 200);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(18, 20);
			this.label27.TabIndex = 1;
			this.label27.Text = "×";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label20.Location = new System.Drawing.Point(151, 69);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(18, 20);
			this.label20.TabIndex = 1;
			this.label20.Text = "×";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label31.Location = new System.Drawing.Point(27, 280);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(54, 15);
			this.label31.TabIndex = 1;
			this.label31.Text = "Location";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label22.Location = new System.Drawing.Point(151, 122);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(18, 20);
			this.label22.TabIndex = 1;
			this.label22.Text = "×";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label26.Location = new System.Drawing.Point(27, 202);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(54, 15);
			this.label26.TabIndex = 1;
			this.label26.Text = "Location";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label18.Location = new System.Drawing.Point(28, 71);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(31, 15);
			this.label18.TabIndex = 1;
			this.label18.Text = "Size";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label21.Location = new System.Drawing.Point(27, 124);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(54, 15);
			this.label21.TabIndex = 1;
			this.label21.Text = "Location";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label19.Location = new System.Drawing.Point(151, 44);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(18, 20);
			this.label19.TabIndex = 1;
			this.label19.Text = "×";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(27, 46);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(54, 15);
			this.label17.TabIndex = 1;
			this.label17.Text = "Location";
			// 
			// nudLedOffsetX
			// 
			this.nudLedOffsetX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudLedOffsetX.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.nudLedOffsetX.Location = new System.Drawing.Point(940, 399);
			this.nudLedOffsetX.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
			this.nudLedOffsetX.Name = "nudLedOffsetX";
			this.nudLedOffsetX.Size = new System.Drawing.Size(94, 22);
			this.nudLedOffsetX.TabIndex = 4;
			this.nudLedOffsetX.Value = new decimal(new int[] {
            1280,
            0,
            0,
            0});
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCancel.Location = new System.Drawing.Point(816, 433);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(92, 33);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			// 
			// btnAllSave
			// 
			this.btnAllSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.btnAllSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnAllSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAllSave.Location = new System.Drawing.Point(926, 433);
			this.btnAllSave.Name = "btnAllSave";
			this.btnAllSave.Size = new System.Drawing.Size(92, 33);
			this.btnAllSave.TabIndex = 5;
			this.btnAllSave.Text = "Save";
			this.btnAllSave.UseVisualStyleBackColor = true;
			this.btnAllSave.Click += new System.EventHandler(this.btnAllSave_Click);
			// 
			// label36
			// 
			this.label36.AutoSize = true;
			this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label36.Location = new System.Drawing.Point(771, 399);
			this.label36.Name = "label36";
			this.label36.Size = new System.Drawing.Size(163, 20);
			this.label36.TabIndex = 1;
			this.label36.Text = "LED Output Offset(X)";
			// 
			// nudVideoLocY
			// 
			this.nudVideoLocY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nudVideoLocY.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.nudVideoLocY.Location = new System.Drawing.Point(176, 277);
			this.nudVideoLocY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.nudVideoLocY.Name = "nudVideoLocY";
			this.nudVideoLocY.Size = new System.Drawing.Size(59, 22);
			this.nudVideoLocY.TabIndex = 4;
			this.nudVideoLocY.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
			// 
			// SettingsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(1050, 475);
			this.Controls.Add(this.btnAllSave);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label36);
			this.Controls.Add(this.nudLedOffsetX);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SettingsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Settings";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeSponsor)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.toolStrip2.ResumeLayout(false);
			this.toolStrip2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeVideo1)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.toolStrip3.ResumeLayout(false);
			this.toolStrip3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeVideo2)).EndInit();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.toolStrip4.ResumeLayout(false);
			this.toolStrip4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudTimeVideo3)).EndInit();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudVideoSizeH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudMsgSizeH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudAwaySizeH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHomeSizeH)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudVideoSizeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudMsgSizeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudAwaySizeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHomeSizeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudMsgLocY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudAwayLocY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHomeLocY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudVideoLocX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudMsgLocX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudAwayLocX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudHomeLocX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudLedOffsetX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudVideoLocY)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnBrowseSponsor;
		private System.Windows.Forms.TextBox txtFileSponsor;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown nudTimeSponsor;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.ToolStrip toolStrip2;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnBackColorPickVideo1;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnForeColorPickVideo1;
		private System.Windows.Forms.NumericUpDown nudTimeVideo1;
		private System.Windows.Forms.TextBox txtNameVideo1;
		private System.Windows.Forms.TextBox txtFileVideo1;
		private System.Windows.Forms.Button btnBrowseVideo1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.ToolStrip toolStrip3;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnBackColorPickVideo2;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnForeColorPickVideo2;
		private System.Windows.Forms.NumericUpDown nudTimeVideo2;
		private System.Windows.Forms.TextBox txtNameVideo2;
		private System.Windows.Forms.TextBox txtFileVideo2;
		private System.Windows.Forms.Button btnBrowseVideo2;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.ToolStrip toolStrip4;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnBackColorPickVideo3;
		private Cyotek.Windows.Forms.ToolStripControllerHosts.ToolStripColorPickerSplitButton btnForeColorPickVideo3;
		private System.Windows.Forms.NumericUpDown nudTimeVideo3;
		private System.Windows.Forms.TextBox txtNameVideo3;
		private System.Windows.Forms.TextBox txtFileVideo3;
		private System.Windows.Forms.Button btnBrowseVideo3;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.NumericUpDown nudHomeLocX;
		private System.Windows.Forms.NumericUpDown nudHomeSizeH;
		private System.Windows.Forms.NumericUpDown nudHomeSizeW;
		private System.Windows.Forms.NumericUpDown nudHomeLocY;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.NumericUpDown nudAwaySizeH;
		private System.Windows.Forms.NumericUpDown nudAwaySizeW;
		private System.Windows.Forms.NumericUpDown nudAwayLocY;
		private System.Windows.Forms.NumericUpDown nudAwayLocX;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.NumericUpDown nudVideoSizeH;
		private System.Windows.Forms.NumericUpDown nudMsgSizeH;
		private System.Windows.Forms.NumericUpDown nudVideoSizeW;
		private System.Windows.Forms.NumericUpDown nudMsgSizeW;
		private System.Windows.Forms.NumericUpDown nudLedOffsetX;
		private System.Windows.Forms.NumericUpDown nudMsgLocY;
		private System.Windows.Forms.NumericUpDown nudVideoLocX;
		private System.Windows.Forms.NumericUpDown nudMsgLocX;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Button btnAreaSave;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnAllSave;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.NumericUpDown nudVideoLocY;
	}
}