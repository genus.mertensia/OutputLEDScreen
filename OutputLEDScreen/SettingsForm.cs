﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutputLEDScreen
{
	partial class SettingsForm : Form
	{
		private string[] _videoFiles = new string[4];
		private bool _editableArea;
		private SettingsInfo _info;

		#region Overriding

		private SettingsForm()
		{
			InitializeComponent();
		}

		public SettingsForm(SettingsInfo s)
		{
			InitializeComponent();

			_info = s;

			nudLedOffsetX.Value = _info.LEDOffset.X;

			// items info
			txtFileSponsor.Text = s.ItemInfos[0].dirPath;
			nudTimeSponsor.Value = s.ItemInfos[0].displayTime;

			txtFileVideo1.Text = Path.GetFileName(s.ItemInfos[1].dirPath);
			nudTimeVideo1.Value = s.ItemInfos[1].displayTime;
			txtNameVideo1.Text = s.ItemInfos[1].displayName;
			txtNameVideo1.BackColor = s.ItemInfos[1].backColor;
			btnBackColorPickVideo1.Color = s.ItemInfos[1].backColor;
			txtNameVideo1.ForeColor = s.ItemInfos[1].foreColor;
			btnForeColorPickVideo1.Color = s.ItemInfos[1].foreColor;

			_videoFiles[1] = s.ItemInfos[1].dirPath;

			txtFileVideo2.Text = Path.GetFileName(s.ItemInfos[2].dirPath);
			nudTimeVideo2.Value = s.ItemInfos[2].displayTime;
			txtNameVideo2.Text = s.ItemInfos[2].displayName;
			txtNameVideo2.BackColor = s.ItemInfos[2].backColor;
			btnBackColorPickVideo2.Color = s.ItemInfos[2].backColor;
			txtNameVideo2.ForeColor = s.ItemInfos[2].foreColor;
			btnForeColorPickVideo2.Color = s.ItemInfos[2].foreColor;

			_videoFiles[2] = s.ItemInfos[2].dirPath;

			txtFileVideo3.Text = Path.GetFileName(s.ItemInfos[3].dirPath);
			nudTimeVideo3.Value = s.ItemInfos[3].displayTime;
			txtNameVideo3.Text = s.ItemInfos[3].displayName;
			txtNameVideo3.BackColor = s.ItemInfos[3].backColor;
			btnBackColorPickVideo3.Color = s.ItemInfos[3].backColor;
			txtNameVideo3.ForeColor = s.ItemInfos[3].foreColor;
			btnForeColorPickVideo3.Color = s.ItemInfos[3].foreColor;

			_videoFiles[3] = s.ItemInfos[3].dirPath;

			// areas info
			nudHomeLocX.Value = s.AreaInfos[0].location.X;
			nudHomeLocY.Value = s.AreaInfos[0].location.Y;
			nudHomeSizeW.Value = s.AreaInfos[0].size.Width;
			nudHomeSizeH.Value = s.AreaInfos[0].size.Height;

			nudAwayLocX.Value = s.AreaInfos[1].location.X;
			nudAwayLocY.Value = s.AreaInfos[1].location.Y;
			nudAwaySizeW.Value = s.AreaInfos[1].size.Width;
			nudAwaySizeH.Value = s.AreaInfos[1].size.Height;

			nudMsgLocX.Value = s.AreaInfos[2].location.X;
			nudMsgLocY.Value = s.AreaInfos[2].location.Y;
			nudMsgSizeW.Value = s.AreaInfos[2].size.Width;
			nudMsgSizeH.Value = s.AreaInfos[2].size.Height;

			nudVideoLocX.Value = s.AreaInfos[3].location.X;
			nudVideoLocY.Value = s.AreaInfos[3].location.Y;
			nudVideoSizeW.Value = s.AreaInfos[3].size.Width;
			nudVideoSizeH.Value = s.AreaInfos[3].size.Height;
		}

		protected override void OnLoad(EventArgs e)
		{
			_editableArea = false;

			EnableControls(_editableArea);

			base.OnLoad(e);
		}

		#endregion

		#region Command

		private void EnableControls(bool enabled)
		{
			nudHomeLocX.Enabled = enabled;
			nudHomeLocY.Enabled = enabled;
			nudHomeSizeW.Enabled = enabled;
			nudHomeSizeH.Enabled = enabled;

			nudAwayLocX.Enabled = enabled;
			nudAwayLocY.Enabled = enabled;
			nudAwaySizeW.Enabled = enabled;
			nudAwaySizeH.Enabled = enabled;

			nudMsgLocX.Enabled = enabled;
			nudMsgLocY.Enabled = enabled;
			nudMsgSizeW.Enabled = enabled;
			nudMsgSizeH.Enabled = enabled;

			nudVideoLocX.Enabled = enabled;
			nudVideoLocY.Enabled = enabled;
			nudVideoSizeW.Enabled = enabled;
			nudVideoSizeH.Enabled = enabled;

			if (!_editableArea)
				btnAreaSave.Text = "Enable Editing";
			else
				btnAreaSave.Text = "Save Display Area";
		}

		private void btnAllSave_Click(object sender, EventArgs e)
		{
			_info.LEDOffset = new Point((int)nudLedOffsetX.Value, 0);

			// items info
			_info.ItemInfos[0].dirPath = txtFileSponsor.Text;
			_info.ItemInfos[0].displayTime = (int)nudTimeSponsor.Value;

			_info.ItemInfos[1].dirPath = _videoFiles[1];
			_info.ItemInfos[1].displayTime = (int)nudTimeVideo1.Value;
			_info.ItemInfos[1].displayName = txtNameVideo1.Text;
			_info.ItemInfos[1].backColor = txtNameVideo1.BackColor;
			_info.ItemInfos[1].backColor = btnBackColorPickVideo1.Color;
			_info.ItemInfos[1].foreColor = txtNameVideo1.ForeColor;
			_info.ItemInfos[1].foreColor = btnForeColorPickVideo1.Color;

			_info.ItemInfos[2].dirPath = _videoFiles[2];
			_info.ItemInfos[2].displayTime = (int)nudTimeVideo2.Value;
			_info.ItemInfos[2].displayName = txtNameVideo2.Text;
			_info.ItemInfos[2].backColor = txtNameVideo2.BackColor;
			_info.ItemInfos[2].backColor = btnBackColorPickVideo2.Color;
			_info.ItemInfos[2].foreColor = txtNameVideo2.ForeColor;
			_info.ItemInfos[2].foreColor = btnForeColorPickVideo2.Color;

			_info.ItemInfos[3].dirPath = _videoFiles[3];
			_info.ItemInfos[3].displayTime = (int)nudTimeVideo3.Value;
			_info.ItemInfos[3].displayName = txtNameVideo3.Text;
			_info.ItemInfos[3].backColor = txtNameVideo3.BackColor;
			_info.ItemInfos[3].backColor = btnBackColorPickVideo3.Color;
			_info.ItemInfos[3].foreColor = txtNameVideo3.ForeColor;
			_info.ItemInfos[3].foreColor = btnForeColorPickVideo3.Color;

			// areas info
			_info.AreaInfos[0].location.X = (int)nudHomeLocX.Value;
			_info.AreaInfos[0].location.Y = (int)nudHomeLocY.Value;
			_info.AreaInfos[0].size.Width = (int)nudHomeSizeW.Value;
			_info.AreaInfos[0].size.Height = (int)nudHomeSizeH.Value;

			_info.AreaInfos[1].location.X = (int)nudAwayLocX.Value;
			_info.AreaInfos[1].location.Y = (int)nudAwayLocY.Value;
			_info.AreaInfos[1].size.Width = (int)nudAwaySizeW.Value;
			_info.AreaInfos[1].size.Height = (int)nudAwaySizeH.Value;

			_info.AreaInfos[2].location.X = (int)nudMsgLocX.Value;
			_info.AreaInfos[2].location.Y = (int)nudMsgLocY.Value;
			_info.AreaInfos[2].size.Width = (int)nudMsgSizeW.Value;
			_info.AreaInfos[2].size.Height = (int)nudMsgSizeH.Value;

			_info.AreaInfos[3].location.X = (int)nudVideoLocX.Value;
			_info.AreaInfos[3].location.Y = (int)nudVideoLocY.Value;
			_info.AreaInfos[3].size.Width = (int)nudVideoSizeW.Value;
			_info.AreaInfos[3].size.Height = (int)nudVideoSizeH.Value;

			this.DialogResult = DialogResult.OK;
		}

		private void btnSizeSave_Click(object sender, EventArgs e)
		{
			_editableArea = !_editableArea;
			EnableControls(_editableArea);
		}

		#endregion

		#region Color Change

		private void btnBackColorPickVideo1_ColorChanged(object sender, EventArgs e)
		{
			txtNameVideo1.BackColor = btnBackColorPickVideo1.Color;
		}

		private void btnForeColorPickVideo1_ColorChanged(object sender, EventArgs e)
		{
			txtNameVideo1.ForeColor = btnForeColorPickVideo1.Color;
		}

		private void btnBackColorPickVideo2_ColorChanged(object sender, EventArgs e)
		{
			txtNameVideo2.BackColor = btnBackColorPickVideo2.Color;
		}

		private void btnForeColorPickVideo2_ColorChanged(object sender, EventArgs e)
		{
			txtNameVideo2.ForeColor = btnForeColorPickVideo2.Color;
		}

		private void btnBackColorPickVideo3_ColorChanged(object sender, EventArgs e)
		{
			txtNameVideo3.BackColor = btnBackColorPickVideo3.Color;
		}

		private void btnForeColorPickVideo3_ColorChanged(object sender, EventArgs e)
		{
			txtNameVideo3.ForeColor = btnForeColorPickVideo3.Color;
		}

		#endregion

		#region Set Folder Browse

		private void btnBrowseSponsor_Click(object sender, EventArgs e)
		{
			try
			{
				FolderBrowserDialog dlg = new FolderBrowserDialog();
				dlg.Description = "Select folder";
				dlg.SelectedPath = txtFileSponsor.Text;
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					txtFileSponsor.Text = dlg.SelectedPath;
				}
			}
			catch { }
		}

		private void btnBrowseVideo1_Click(object sender, EventArgs e)
		{
			try
			{
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.Title = "Select Video File(1)";
				dlg.InitialDirectory = txtFileVideo1.Text;
				dlg.Filter = "All Video files (*.*)|*.*";
				dlg.RestoreDirectory = true;
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					_videoFiles[1] = dlg.FileName;
					txtFileVideo1.Text = Path.GetFileName(dlg.FileName);
				}
			}
			catch { }
		}

		private void btnBrowseVideo2_Click(object sender, EventArgs e)
		{
			try
			{
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.Title = "Select Video File(2)";
				dlg.InitialDirectory = txtFileVideo1.Text;
				dlg.Filter = "All Video files (*.*)|*.*";
				dlg.RestoreDirectory = true;
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					_videoFiles[2] = dlg.FileName;
					txtFileVideo2.Text = Path.GetFileName(dlg.FileName);
				}
			}
			catch { }
		}

		private void btnBrowseVideo3_Click(object sender, EventArgs e)
		{
			try
			{
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.Title = "Select Video File(3)";
				dlg.InitialDirectory = txtFileVideo1.Text;
				dlg.Filter = "All Video files (*.*)|*.*";
				dlg.RestoreDirectory = true;
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					_videoFiles[3] = dlg.FileName;
					txtFileVideo3.Text = Path.GetFileName(dlg.FileName);
				}
			}
			catch { }
		}

		#endregion
	}
}
