﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OutputLEDScreen
{
	public struct ScreenTextInfo
	{
		public string displayText;
		public string fontName;
		//public float fontSize;
		//public FontStyle fontStyle;
		public Color backColor;
		public Color foreColor;
		public int scrollSpeed;
	}

	public struct DisplayItemInfo
	{
		public string dirPath;
		public int displayTime;
		public string displayName;
		public Color backColor;
		public Color foreColor;
	}

	public struct DisplayAreaInfo
	{
		public Point location;
		public Size size;
	}


	public class SettingsInfo
	{
		private ScreenTextInfo[] _textInfos;
		public ScreenTextInfo[] TextInfos
		{ get { return _textInfos; } }

		private DisplayItemInfo[] _itemInfos;
		public DisplayItemInfo[] ItemInfos
		{ get { return _itemInfos; } }

		private DisplayAreaInfo[] _areaInfos;
		public DisplayAreaInfo[] AreaInfos
		{ get { return _areaInfos; } }

		private Point _ledOffset;
		public Point LEDOffset
		{
			get { return _ledOffset; }
			set { _ledOffset = value; }
		}

		public SettingsInfo()
		{
			_ledOffset = new Point(1280, 0);

			_textInfos = new ScreenTextInfo[3];
			_itemInfos = new DisplayItemInfo[4];
			_areaInfos = new DisplayAreaInfo[4];

			// setting default value;
			for (int i = 0; i < 3; i++)
			{
				_textInfos[i].displayText = "User Input Text Box";
				_textInfos[i].fontName = "Microsoft Sans Serif";
				//_textInfos[i].fontSize = 28;
				//_textInfos[i].fontStyle = FontStyle.Regular;
				_textInfos[i].backColor = Color.White;
				_textInfos[i].foreColor = Color.Black;
				_textInfos[i].scrollSpeed = 7;
			}

			for (int i = 0; i < 4; i++)
			{
				_itemInfos[i].dirPath = "";
				_itemInfos[i].displayTime = 7;
				_itemInfos[i].displayName = "Video" + i.ToString();
				_itemInfos[i].backColor = Color.White;
				_itemInfos[i].foreColor = Color.Black;
			}

			_areaInfos[0].location = new Point(0, 0);
			_areaInfos[0].size = new Size(160, 32);

			_areaInfos[1].location = new Point(0, 32);
			_areaInfos[1].size = new Size(160, 32);

			_areaInfos[2].location = new Point(0, 64);
			_areaInfos[2].size = new Size(480, 32);

			_areaInfos[3].location = new Point(0, 96);
			_areaInfos[3].size = new Size(512, 128);

			Load();
		}

		private static string DataFileName
		{
			get { return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Settings.xml"); }
		}

		private void Load()
		{
			string fileName = DataFileName;
			int index0 = 0;
			int index1 = 0;
			int index2 = 0;
			string parentName = ""; 

			XmlTextReader xmlReader = null;
			try
			{
				xmlReader = new XmlTextReader(fileName);

				while (xmlReader.Read())
				{
					if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "LED")
					{
						parentName = "LED";
						continue;
					}

					if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Texts")
					{
						parentName = "Texts";
						continue;
					}

					if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Items")
					{
						parentName = "Items";
						continue;
					}

					if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Areas")
					{
						parentName = "Areas";
						continue;
					}

					if (parentName == "LED")
					{
						if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Data")
						{
							Point pos = new Point(1280, 0);

							while (xmlReader.MoveToNextAttribute())
							{
								if (xmlReader.Name == "OffsetX")
								{
									if (xmlReader.Value != string.Empty)
										pos.X = int.Parse(xmlReader.Value);
									else
										pos.X = 1280;
								}
							}

							_ledOffset = pos;
						}
					}
					else if (parentName == "Texts")
					{
						if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Data")
						{
							string displayText = "";
							string fontName = "";
							//float fontSize = 0;
							//FontStyle fontStyle = FontStyle.Regular;
							Color backColor = Color.White;
							Color foreColor = Color.Black;
							int scrollSpeed = 7;

							while (xmlReader.MoveToNextAttribute())
							{
								if (xmlReader.Name == "DisplayText")
									displayText = xmlReader.Value;
								else if (xmlReader.Name == "FontName")
									fontName = xmlReader.Value;
								/*else if (xmlReader.Name == "FontSize")
								{
									if (xmlReader.Value != string.Empty)
										fontSize = float.Parse(xmlReader.Value);
									else
										fontSize = 0;
								}
								else if (xmlReader.Name == "FontStyle")
								{
									if (xmlReader.Value != string.Empty)
										fontStyle = (FontStyle)int.Parse(xmlReader.Value);
									else
										fontStyle = 0;
								}*/
								else if (xmlReader.Name == "BackColor")
									backColor = Color.FromArgb(int.Parse(xmlReader.Value));
								else if (xmlReader.Name == "ForeColor")
									foreColor = Color.FromArgb(int.Parse(xmlReader.Value));
								else if (xmlReader.Name == "ScrollSpeed")
								{
									if (xmlReader.Value != string.Empty)
										scrollSpeed = int.Parse(xmlReader.Value);
									else
										scrollSpeed = 0;
								}
							}

							if (index0 < 3)
							{
								if (fontName == string.Empty)
									fontName = "Microsoft Sans Serif";

								if (scrollSpeed > 20)
									scrollSpeed = 20;
								else if (scrollSpeed < 0)
									scrollSpeed = 0;

								_textInfos[index0].displayText = displayText;
								_textInfos[index0].fontName = fontName;
								//_textInfos[index0].fontSize = fontSize;
								//_textInfos[index0].fontStyle = fontStyle;
								_textInfos[index0].backColor = backColor;
								_textInfos[index0].foreColor = foreColor;
								_textInfos[index0].scrollSpeed = scrollSpeed;

								index0++;
							}
						}
					}
					if (parentName == "Items")
					{
						if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Data")
						{
							string dirPath = "";
							int displayTime = 0;
							string displayName = "";
							Color backColor = Color.White;
							Color foreColor = Color.Black;

							while (xmlReader.MoveToNextAttribute())
							{
								if (xmlReader.Name == "DirPath")
									dirPath = xmlReader.Value;
								else if (xmlReader.Name == "DisplayTime")
								{
									if (xmlReader.Value != string.Empty)
										displayTime = int.Parse(xmlReader.Value);
									else
										displayTime = 0;
								}
								else if (xmlReader.Name == "DisplayName")
									displayName = xmlReader.Value;
								else if (xmlReader.Name == "BackColor")
									backColor = Color.FromArgb(int.Parse(xmlReader.Value));
								else if (xmlReader.Name == "ForeColor")
									foreColor = Color.FromArgb(int.Parse(xmlReader.Value));
							}

							if (index1 < 4)
							{
								_itemInfos[index1].dirPath = dirPath;
								_itemInfos[index1].displayTime = displayTime;
								_itemInfos[index1].displayName = displayName;
								_itemInfos[index1].backColor = backColor;
								_itemInfos[index1].foreColor = foreColor;

								index1++;
							}
						}
					}
					if (parentName == "Areas")
					{
						if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Data")
						{
							Point location = new Point(0, 0);
							Size size = new Size(160, 32);

							while (xmlReader.MoveToNextAttribute())
							{
								if (xmlReader.Name == "X")
								{
									if (xmlReader.Value != string.Empty)
										location.X = int.Parse(xmlReader.Value);
									else
										location.X = 0;
								}
								else if (xmlReader.Name == "Y")
								{
									if (xmlReader.Value != string.Empty)
										location.Y = int.Parse(xmlReader.Value);
									else
										location.Y = 0;
								}
								else if (xmlReader.Name == "Width")
								{
									if (xmlReader.Value != string.Empty)
										size.Width = int.Parse(xmlReader.Value);
									else
										size.Width = 0;
								}
								else if (xmlReader.Name == "Height")
								{
									if (xmlReader.Value != string.Empty)
										size.Height = int.Parse(xmlReader.Value);
									else
										size.Height = 0;
								}
							}

							if (index2 < 4)
							{
								_areaInfos[index2].location = location;
								_areaInfos[index2].size = size;

								index2++;
							}
						}
					}
				}
			}
			catch
			{
				Console.WriteLine("Settings.xml not found");
				return;
			}
			finally
			{
				// Finished with XmlTextReader
				if (xmlReader != null)
					xmlReader.Close();
			}
		}

		public void Save()
		{
			string fileName = DataFileName;

			XmlWriter xmlWriter = null;
			try
			{
				XmlWriterSettings settings = new XmlWriterSettings();
				settings.Indent = true;
				settings.IndentChars = "\t";
				settings.OmitXmlDeclaration = false;
				settings.Encoding = Encoding.UTF8;

				xmlWriter = XmlWriter.Create(fileName, settings);

				xmlWriter.WriteStartElement("Settings");

				xmlWriter.WriteStartElement("LED");

				xmlWriter.WriteStartElement("Data");
				xmlWriter.WriteAttributeString("OffsetX", _ledOffset.X.ToString());
				xmlWriter.WriteEndElement();

				xmlWriter.WriteEndElement();

				xmlWriter.WriteStartElement("Texts");

				for (int i = 0; i < _textInfos.Length; i++)
				{
					xmlWriter.WriteStartElement("Data");

					xmlWriter.WriteAttributeString("DisplayText", _textInfos[i].displayText);
					xmlWriter.WriteAttributeString("FontName", _textInfos[i].fontName);
					//xmlWriter.WriteAttributeString("FontSize", _textInfos[i].fontSize.ToString());
					//xmlWriter.WriteAttributeString("FontStyle", ((int)_textInfos[i].fontStyle).ToString());
					xmlWriter.WriteAttributeString("BackColor", _textInfos[i].backColor.ToArgb().ToString());
					xmlWriter.WriteAttributeString("ForeColor", _textInfos[i].foreColor.ToArgb().ToString());
					xmlWriter.WriteAttributeString("ScrollSpeed", _textInfos[i].scrollSpeed.ToString());

					xmlWriter.WriteEndElement();
				}

				xmlWriter.WriteEndElement();

				xmlWriter.WriteStartElement("Items");

				for (int i = 0; i < _itemInfos.Length; i++)
				{
					xmlWriter.WriteStartElement("Data");

					xmlWriter.WriteAttributeString("DirPath", _itemInfos[i].dirPath);
					xmlWriter.WriteAttributeString("DisplayTime", _itemInfos[i].displayTime.ToString());
					xmlWriter.WriteAttributeString("DisplayName", _itemInfos[i].displayName);
					xmlWriter.WriteAttributeString("BackColor", _itemInfos[i].backColor.ToArgb().ToString());
					xmlWriter.WriteAttributeString("ForeColor", _itemInfos[i].foreColor.ToArgb().ToString());

					xmlWriter.WriteEndElement();
				}

				xmlWriter.WriteEndElement();

				xmlWriter.WriteStartElement("Areas");

				for (int i = 0; i < _areaInfos.Length; i++)
				{
					xmlWriter.WriteStartElement("Data");

					xmlWriter.WriteAttributeString("X", _areaInfos[i].location.X.ToString());
					xmlWriter.WriteAttributeString("Y", _areaInfos[i].location.Y.ToString());
					xmlWriter.WriteAttributeString("Width", _areaInfos[i].size.Width.ToString());
					xmlWriter.WriteAttributeString("Height", _areaInfos[i].size.Height.ToString());

					xmlWriter.WriteEndElement();
				}

				xmlWriter.WriteEndElement();

				xmlWriter.WriteEndElement();
			}
			catch
			{
				Console.WriteLine("Settings.xml write error");
				return;
			}
			finally
			{
				// Finished with XmlTextReader
				if (xmlWriter != null)
					xmlWriter.Close();
			}
		}
	}
}
